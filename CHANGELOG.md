# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/), and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [0.4.1] - 2025-1-20

This is the working version for the successful Serenity x Apollo slice test

### Changed
- Versions Set:
  - **EMP-Herd** = master-407b22b5

## [0.4.0-tcds2v02] - 2025-2-12

### Fixed

- Bug in Pin YAML files pointing to the wrong FPGA

- Bug in optics.cpp which was reading the wrong registers when checking for a given FF being present

### Added

- Example link test bash scripts for EMPButler and Herd. Also example input_data with counter for easy debugging.

### Changed
- Versions Set:
  - **EMP-Herd** = tcds2-v0-2-c277c601

## [0.3.0] - 2024-12-10

### Fixed
- Before we throw runtime errors, always remove the stream associated to the current ApolloSM to avoid kernel panic when starting herd instance again.

- **Power** commands now more robust to erroneous inputs and have longer up and down times by default. No longer have the option to power on CM2 (only one CM)

### Added
- **devcontainer.json** for developing inside of container directly

- **UIODaemon** functionality for programming FPGAs instead of BUTool's SVFPlayer

- **PinParsing**
  - **ParseSpec**: Dummy scripts for reading YAML configuration files
  - **toYAML**: Converting information from Pin files in Rev2b repo into a more human-readable YAML file

- Pin YAML files used in defining rx and tx links in the ApolloCMFPGA and Firefly/Optics classes

- **ApolloCMFPGA** has logic (defined in utilities.cpp) for registering rx and tx links by reading the Pin YAML files

- **Firefly** functionality implemented which properly detects connected fireflies, monitors their optical power and temperature, and registers them with EMP

- **Program.yml** tracks register names which are read when programming the FPGAs

- **Frontpanel.txt** dummy for now

- **Program** now has AXI/AXILite register reads to make sure the AXI connections to the FPGA are up after programming. Also reads a random register on the FPGA as a test.

- **dumpFFInfo** is a new command which dumps all link information after it is registered by the ApolloCMFPGA in case debugging is needed

- **entrypoint_env.sh**: Added global variables for the FPGA model (ex: VU13p) and Apollo type (ex: Rev2b)

### Changed
- **CentOS7** deprecated, moving to Alma8 Arm64
  - Switch from yum to dnf

- **Apollo.yml** uses symlinked, informative name of the UIO devices instead of the ambiguous numbered name

- Versions Set:
  - **UHAL** = Now taken from parent emp-herd image
  - **UIOUHAL** = 2.0.0
  - **ApolloTool** = 3.2.1
  - **EMP-Herd** = master-407b22b5
  - **Herd-App** = v0.4.25
  - **Swatch** = 1.7.0
  - **EMP-Swatch** = 0.4.0

## [0.2.0] - 2023-02-2

### Fixed
- **install_dependencies.sh**: Building BUTool skipping linter step to avoid compilation issues

### Added

- Added the abilitiy to use the ApolloSM to read registers on the SM + MCU (FPGAs if they are programmed)
    - **ApolloDeviceController** - Some helper classes to read registers which return different types of data
    - **ApolloMetric** - Object to define monitorable parameters
    - **ApolloTable** - Object which holds various ApolloMetrics (specifically designed for the BUTool status command)
    - **ApolloDevice** - Added ApolloTable member defining all BUTool status tables we wish to monitor

- **install_dependencies.sh**: Added systemd-devel to dependency list for uHAL and UIOuHAL

### Changed
- Versions Set:
  - **ApolloTool** = 2.1.1
  - **EMP-Herd** = 0.3.11
  - **Herd-App** = 0.3.12
  - **EMP-Swatch** = 0.3.7

- **Program**: Updated to be compatible with new EMP-Herd handling of the tarball used to program the FPGAs.
  - XVC and address table path no longer registered parameters, instead they are hard-coded into the script.
  - Additional status information


## [0.1.1-dummyFF] - 2022-01-27

### Added

Dummy implementation of Optics and FFs (Optical Module) classes. 


## [0.1.0] - 2022-01-25

### Added

- Base Apollo Herd plugin functionality:
  - **ApolloCMFPGA** (Processor)
  - **ApolloDevice** (Service Module)
  - **Commands**: Program, PowerUp/Down, ApolloAccess
  - **install_dependencies** compatible with CentOS7

- Versions Set:
  - **UHAL** = 2.8.1
  - **UIOUHAL** = 1.1
  - **ApolloTool** = 1.7
  - **EMP-Herd** = 0.3.4
  - **Herd-App** = 0.3.5
  - **Swatch** = 1.5.0
  - **EMP-Swatch** = 0.3.2


