import re
import yaml

file_name = 'PinInfo_Rev2b_VU13p_p1.txt'
with open(file_name, 'r') as file:
    input_text = file.read()

pattern = r'set_property PACKAGE_PIN\s+([A-Z0-9]+)\s+\[get_ports\s+{\s+([pn])_([a-zA-Z0-9_]+)_(xmit|recv)\[(\d+)\]\s+}\s+]' #For Rev2b

matches = re.findall(pattern, input_text)
print(f"Number of matches: {len(matches)}")

set_property_lines = sum(1 for line in input_text.split('\n') if line.strip().startswith('set_property'))
print(f"Number of lines starting with set_property: {set_property_lines}")

yaml_data = {}

for match in matches:
    pin, sig_diff, identifier, direction, index = match

    if identifier.startswith('ff'):
        print(pin, sig_diff, identifier, direction, index)
        if 'firefly' not in yaml_data:
            yaml_data['firefly'] = {}
        firefly_num = int(identifier[2:])
        if firefly_num not in yaml_data['firefly']:
            yaml_data['firefly'][firefly_num] = {}
        if index not in yaml_data['firefly'][firefly_num]:
            yaml_data['firefly'][firefly_num][index] = {}
        if direction not in yaml_data['firefly'][firefly_num][index]:
            yaml_data['firefly'][firefly_num][index][direction] = {}
        yaml_data['firefly'][firefly_num][index][direction][sig_diff] = {}
        yaml_data['firefly'][firefly_num][index][direction][sig_diff]['TraceName'] = pin

    else:
        if 'gty' not in yaml_data:
            yaml_data['gty'] = {}
        gty_num = identifier
        if gty_num not in yaml_data['gty']:
            yaml_data['gty'][gty_num] = {}
        if index not in yaml_data['gty'][gty_num]:
            yaml_data['gty'][gty_num][index] = {}
        if direction not in yaml_data['gty'][gty_num][index]:
            yaml_data['gty'][gty_num][index][direction] = {}
        yaml_data['gty'][gty_num][index][direction][sig_diff] = {}
        yaml_data['gty'][gty_num][index][direction][sig_diff]['TraceName'] = pin

prefix = ""
if 'Rev2b' in file_name:
    if file_name.endswith('p1.txt'):
        prefix = 'F1_'
    elif file_name.endswith('p2.txt'):
        prefix = 'F2_'

if 'firefly' in yaml_data:
    updated_firefly_data = {}
    for firefly_num, firefly_info in yaml_data['firefly'].items():
        updated_firefly_num = f"{prefix}{firefly_num}"
        updated_firefly_data[updated_firefly_num] = firefly_info
    yaml_data['firefly'] = updated_firefly_data

if 'gty' in yaml_data:
    updated_gty_data = {}
    for gty_num, gty_info in yaml_data['gty'].items():
        updated_gty_num = f"{prefix}{gty_num}"
        updated_gty_data[updated_gty_num] = gty_info
    yaml_data['gty'] = updated_gty_data

def count_lowest_level_nodes(data):
    count = 0
    for item in data.values():
        if isinstance(item, dict):
            count += count_lowest_level_nodes(item)
        else:
            count += 1
    return count

lowest_level_count = count_lowest_level_nodes(yaml_data)
print(f"Number of lowest level nodes: {lowest_level_count}")

with open('PinInfo_2b_VU13p_p1.yaml', 'w') as file:
    yaml.dump(yaml_data, file, default_flow_style=False)
