# Apollo-HERD Code Internals

This section of the documentation describes the internal workings of the `apollo-herd` software, together with the underlying `HERD` and `SWATCH` libraries, and `ApolloTool`.

## Contents
[[_TOC_]]

## HERD Configuration File

The `herd` application reads a YAML configuration file, which specifies what types of devices to instantiate, and their properties. These properties include an `id`, `address_table` and the C++ class that represents the device within the `apollo-herd` software. An example configuration file (the default used by `apollo-herd`) can be found [here](https://gitlab.cern.ch/cms-tracker-phase2-onlinesw/apollo-herd/-/blob/master/Apollo.yml).

Detailed information on configuration file structure is provided [here](https://cms-l1t-phase2.docs.cern.ch/online-sw/plugin-development/config-file.html). The users are recommended to consult this documentation, before updating, or creating a configuration file to be used by the `herd` application.

## Classes
The `apollo-herd` plugin library contains three main classes:

* [`ApolloDeviceController`](https://gitlab.cern.ch/cms-tracker-phase2-onlinesw/apollo-herd/-/blob/master/include/swatch/apolloherd/ApolloDeviceController.hpp)

  * This class contains a pointer to a `BUTool::ApolloSMDevice` instance, which it uses to invoke `BUTool` and `ApolloSM` control commands.
  * Contains an `ApolloAccess()` method which gets passed a string containing the space-separated `BUTool` or `ApolloSMDevice` command and arguments, which in turn gets parsed into a vector of strings to be sent to `BUTool::EvaluateCommand` for execution. 
  * Contains an `AddStream()` method, which wraps the `ApolloSMDevice` command `AddOutputStream()` method inherited from BUTextIO. See a more in-depth explanation [here](https://bu-edf.gitlab.io/BUTool/Code_Internals/Class_Hierarchy/).

* [`ApolloDevice`](https://gitlab.cern.ch/cms-tracker-phase2-onlinesw/apollo-herd/-/blob/master/include/swatch/apolloherd/ApolloDevice.hpp)

  * This class simply holds an instance of the `ApolloDeviceController`, and actually invokes the `ApolloAccess()` command, which wraps the SWATCH framework's command invocation methods to call `ApolloDeviceController::ApolloAccess()`

* [`ApolloCMFPGA`](https://gitlab.cern.ch/cms-tracker-phase2-onlinesw/apollo-herd/-/blob/master/include/swatch/apolloherd/ApolloCMFPGA.hpp)

    * This class represents the two main FPGAs on the Command Module, specifically the Kintex and Virtex. It inherits from the `emp::swatch::Processor` [class](https://gitlab.cern.ch:8443/p2-xware/software/emp-herd/-/blob/master/include/emp/swatch/Processor.hpp) from the `emp-herd` plugin, to gain access to commands used for controlling the EMP firmware framework on those FPGAs.
    * Uses the [`FPGA`](https://gitlab.cern.ch/cms-tracker-phase2-onlinesw/apollo-herd/-/blob/master/include/swatch/apolloherd/FPGA.hpp) enum to classify whether the class instance represents the Kintex or Virtex
    * Registers three of its own commands for controlling the CM FPGA:
      * `powerUp()` and `powerDown()` - Wraps the `ApolloSMDevice` CM power commands
      * `program()` - Programs the FPGA from a tarball containing an SVF file and the address tables for the EMP firmware

`ApolloDevice` class inherits from the `phase2::ServiceModule` class from the SWATCH library. This device class gets passed an `swatch::core::AbstractStub` object in it's constructor, which holds information about the `id`, `uri` and `address_table` of the device. Within it's constructor, `ApolloDevice` class then instantiates a `ApolloDeviceController` object, with the `uri` and `address_table` information, which is used for interacting with underyling `ApolloTool` functionalities.

## Commands
For each device registered to the `SWATCH` framework, the commands associated with this device must be registered as well. Within the `SWATCH` framework, the commands are defined as their own classes, which inherit from `swatch::action::Command`, and have the following member function, which has the logic to execute the command:

```cpp
swatch::action::Comand::State code(const swatch::core::ParameterSet& parameters);
```

Once the command class is implemented, it can be registered in the relevant device constructor, using the following expression:

```cpp
// Register the command with the associated class and a command name
registerCommand<MyCommandClass>("myCommand");
```

As an example from `apollo-herd` itself, the `ApolloAccess` command for the `ApolloDevice` can be registered in the device constructor as follows:

```cpp
auto& ApolloAccess = registerCommand<commands::ApolloAccess>("ApolloAccess");
```

The documentation [here](https://cms-l1t-phase2.docs.cern.ch/online-sw/plugin-development/controls/commands.html) has extensive information about how the command classes should be implemented and be registered to the `SWATCH` framework.

## Gitlab CI Pipeline and the Image-Building Process
The `apollo-herd` plugin image gets built for varying architectures by a CI pipeline, which is controlled by the main [HERD control app CI instructions](https://gitlab.cern.ch/cms-tracker-phase2-onlinesw/herd-control-app/-/tree/master/ci). This plugin image then can be run on different blades with different architectures, using the `start_apolloherd.sh` script, as explained [here](https://gitlab.cern.ch/cms-tracker-phase2-onlinesw/deployment-and-documentation#apollo).

### The CI/CD Configuration File
The configuration for the `apollo-herd` CI/CD jobs are specified in the `.gitlab-ci.yml` file, which can be found [here](https://gitlab.cern.ch/cms-tracker-phase2-onlinesw/apollo-herd/-/blob/master/.gitlab-ci.yml). This configuration file points to the CI configuration of the `herd-app` repository with this syntax:

```yaml
# Includes the CI configuration from herd-app, with a specific version (here, v0.3.5)
include:
  - project: cms-tracker-phase2-onlinesw/herd-app
    ref: v0.3.5
    file: /ci/plugin-pipeline.yml
```

### Handling of the Dependencies
During the `apollo-herd` CI job, all dependencies will be installed and built before the main image is built by Docker. This is controlled by the `ci/install_dependencies.sh` script [here](https://gitlab.cern.ch/cms-tracker-phase2-onlinesw/apollo-herd/-/blob/master/ci/install_dependencies.sh). This shell script will download and build the following dependencies:

* [uHAL](https://ipbus.web.cern.ch/)
* [UIOuHAL](https://github.com/BU-Tools/UIOuHAL)
* [ApolloTool](https://github.com/apollo-lhc/ApolloTool)

For the `emp-herd` dependency, a pre-built Docker image is used, instead of installing the source code and building it during the CI job. This is specified in the `.gitlab-ci.yml` file here:

```yaml
# URL and tag of EMP plugin repo (since building on top of that)
BASE_IMAGE_REPO_URL: gitlab-registry.cern.ch/p2-xware/software/emp-herd
BASE_IMAGE_TAG: v0.3.4
```

### More Information
Detailed information on how the CI jobs work, the relevant variables to set to configure the job and the build products are provided in [this](https://cms-l1t-phase2.docs.cern.ch/online-sw/plugin-development/automated-builds.html) documentation.