#ifndef __SWATCH_APOLLOHERD_HELPERS_TIMING_HPP__
#define __SWATCH_APOLLOHERD_HELPERS_TIMING_HPP__

//Timing Stuff
#include <cstdio>
#include <sys/select.h>
#include <sys/time.h>
#include <sys/types.h>
#include <unistd.h>
#include <time.h>


double diff_time(struct timeval t0, struct timeval t1); //timevals
double diff_time(struct timespec t0, struct timespec t1); //timespecs
double diff_time(time_t t0, time_t t1); //time_t
double timeval2float(timeval t0); //convert timeval to float


#endif  // __SWATCH_APOLLOHERD_HELPERS_TIMING_HPP__