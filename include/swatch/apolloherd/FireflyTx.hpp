#ifndef __SWATCH_APOLLOHERD_FIREFLYTX_HPP__
#define __SWATCH_APOLLOHERD_FIREFLYTX_HPP__

#include "swatch/apolloherd/FireflyBase.hpp"

namespace swatch {
namespace apolloherd {

class FireflyTx : public FireflyBase {
public:
  FireflyTx(const std::string& aId);
  virtual ~FireflyTx();

private:

  FireflyTx(const std::string& aId, const std::string& aAlias);

  void retrieveMetricValues() final;

  void retrieveOutputPropertyValues(Channel& aChannel) final;

  void retrieveOutputMetricValues(Channel& aChannel) final;

  std::string FF_name;

  /*
  std::unordered_map<const Channel*, Property<std::string>*> mPropertyState;
  std::unordered_map<const Channel*, Property<std::string>*> mPropertyEqualization;
  std::unordered_map<const Channel*, Property<std::string>*> mPropertyPolarity;
  std::unordered_map<const Channel*, Property<std::string>*> mPropertySquelch;
  std::unordered_map<const Channel*, Property<std::string>*> mPropertyCDR;

  std::unordered_map<const Channel*, SimpleMetric<bool>*> mMetricLOL, mMetricLaserFault;
  */

  static const std::vector<size_t> kChannels;
};


} // apolloherd
} // swatch

#endif //__SWATCH_APOLLOHERD_FIREFLYTX_HPP__
