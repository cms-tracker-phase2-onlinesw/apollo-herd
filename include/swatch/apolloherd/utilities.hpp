#ifndef __SWATCH_APOLLOHERD_UTILITIES_HPP__
#define __SWATCH_APOLLOHERD_UTILITIES_HPP__

#include <string>
#include <memory>
#include <vector>
#include <boost/optional.hpp>
#include "swatch/apolloherd/definitions.hpp"


namespace swatch {
namespace phase2 {
    class OpticalModule;
}
}

namespace swatch {
namespace apolloherd {

struct ChannelOrigin {
    std::map<int, std::pair<Polarity, std::vector<std::string>>> rx;
    std::map<int, std::pair<Polarity, std::vector<std::string>>> tx;
};

ChannelOrigin parseSpec(const CMFPGAType);

CMBoardType getBoardType();

boost::optional<CMFPGAType> getApolloCMFPGAType(const Site);

} // namespace apolloherd
} // namespace swatch


#endif /* __SWATCH_APOLLOHERD_UTILITIES_HPP__ */