#ifndef __SWATCH_APOLLOHERD_APOLLOTABLE_HPP__
#define __SWATCH_APOLLOHERD_APOLLOTABLE_HPP__

#include <algorithm>
#include <inttypes.h>   // int64_t, uint64_t

#include "swatch/core/MonitorableObject.hpp"
#include "swatch/core/SimpleMetric.hpp"

#include "swatch/apolloherd/ApolloMetric.hpp"
#include "swatch/apolloherd/ApolloDeviceController.hpp"

#include "BUException/ExceptionBase.hh"

#include "RegisterHelper/RegisterHelperIO.hh"

typedef BUTool::RegisterHelperIO::ConvertType ConvertType;

namespace swatch {
namespace apolloherd {

class ApolloTable : public swatch::core::MonitorableObject {
public:
    ApolloTable(std::string const & aId, std::string const & aAlias, std::string const & tableName, ApolloDeviceController & controller);
    virtual ~ApolloTable();

    template <typename DataType>
    using SimpleMetric = swatch::core::SimpleMetric<DataType>;

private:
    void retrieveMetricValues() override;
    
    template<typename T>
    void readAndSetMetric(const ApolloMetric & metric);

    std::vector<std::string> mRegisterNames;
    std::vector<ApolloMetric> mApolloMetrics;

    ApolloDeviceController& mController;    

};

} // apolloherd
} // swatch

#endif