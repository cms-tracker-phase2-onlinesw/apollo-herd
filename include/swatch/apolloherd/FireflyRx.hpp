#ifndef __SWATCH_APOLLOHERD_FIREFLYRX_HPP__
#define __SWATCH_APOLLOHERD_FIREFLYRX_HPP__

#include "swatch/apolloherd/FireflyBase.hpp"

namespace swatch {
namespace apolloherd {

class FireflyRx : public FireflyBase {
public:
  FireflyRx(const std::string& aId);
  virtual ~FireflyRx();

private:

  FireflyRx(const std::string& aId, const std::string& aAlias);

  void retrieveInputPropertyValues(Channel& aChannel) final;

  void retrieveMetricValues() final;

  void retrieveInputMetricValues(Channel& aChannel) final;

  std::string FF_name;

  /*
  std::unordered_map<const Channel*, Property<std::string>*> mPropertyState;
  std::unordered_map<const Channel*, Property<std::string>*> mPropertyAmplitude;
  std::unordered_map<const Channel*, Property<std::string>*> mPropertyPolarity;
  std::unordered_map<const Channel*, Property<float>*> mPropertyPreEmph;
  std::unordered_map<const Channel*, Property<std::string>*> mPropertySquelch;
  std::unordered_map<const Channel*, Property<std::string>*> mPropertyCDR;

  std::unordered_map<const Channel*, SimpleMetric<float>*> mMetricPower;
  std::unordered_map<const Channel*, SimpleMetric<bool>*> mMetricLOL, mMetricLOS;
  */

  static const std::vector<size_t> kChannels;

};

} // apolloherd
} // swatch

#endif //__SWATCH_APOLLOHERD_FIREFLYRX_HPP__
