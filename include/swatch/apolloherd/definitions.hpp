#ifndef __SWATCH_APOLLOHERD_DEFINITIONS_HPP__
#define __SWATCH_APOLLOHERD_DEFINITIONS_HPP__

#include <iosfwd>
#include <map>
#include <unordered_map>
#include "swatch/phase2/ChannelID.hpp"

namespace swatch {
namespace apolloherd {

enum class CMFPGAType {
  VU13p_F1,
  VU13p_F2
};

enum class CMBoardType {
  Rev2b
};

enum class Site {
  F1,
  F2
};

enum class Polarity {
  Normal,
  Invert
};

struct DummyComparator {
    bool operator() (const ::swatch::phase2::ChannelID& a, const ::swatch::phase2::ChannelID& b) const {
        return &a < &b; // Compare addresses; doesn't really compare the content.
    }
};

std::string SiteToString(const Site& site);

std::string CMFPGATypeToString(const CMFPGAType& cType);

std::string CMBoardTypeToString(const CMBoardType& cType);

}
}

#endif /* __SWATCH_APOLLOHERD_DEFINITIONS_HPP__ */