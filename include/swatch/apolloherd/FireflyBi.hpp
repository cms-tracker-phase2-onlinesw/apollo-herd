#ifndef __SWATCH_APOLLOHERD_FIREFLYBI_HPP__
#define __SWATCH_APOLLOHERD_FIREFLYBI_HPP__

#include "swatch/apolloherd/FireflyBase.hpp"

namespace swatch {
namespace apolloherd {

class FireflyBi : public FireflyBase {
public:
  FireflyBi(const std::string& aId);
  virtual ~FireflyBi();

private:

  FireflyBi(const std::string& aId, const std::string& aAlias);

  void retrieveMetricValues() final;

  void retrieveInputMetricValues(Channel& aChannel) final;

  void retrieveOutputMetricValues(Channel& aChannel) final;

  /*
  std::unordered_map<const Channel*, SimpleMetric<float>*> mMetricRxPower;
  std::unordered_map<const Channel*, SimpleMetric<bool>*> mMetricRxLOL, mMetricRxLOS;
  std::unordered_map<const Channel*, SimpleMetric<bool>*> mMetricTxLOL, mMetricTxLOS, mMetricTxLaserFault;
  */

  std::string FF_name;

  static const std::vector<size_t> kChannels;
};

} // apolloherd
} // swatch

#endif //__SWATCH_APOLLOHERD_FIREFLYBI_HPP__
