#ifndef __SWATCH_APOLLOHERD_APOLLOCMFPGA_HPP__
#define __SWATCH_APOLLOHERD_APOLLOCMFPGA_HPP__

#include <vector>
#include <string>
#include <algorithm>
#include <iterator>
#include <sstream>

#include "emp/swatch/Processor.hpp"
#include <ApolloSM_device/ApolloSM_device.hh>

#include "swatch/apolloherd/definitions.hpp"
#include "swatch/apolloherd/utilities.hpp"

#include <log4cplus/logger.h>
#include <log4cplus/loggingmacros.h>

namespace swatch {
namespace apolloherd {

class ApolloCMFPGA : public emp::swatch::Processor {
public:

  ApolloCMFPGA(const ::swatch::core::AbstractStub& aStub);
  virtual ~ApolloCMFPGA();

  Site getSite() const;
  Polarity getFireflyRxPolarity(const std::string&, const size_t) const;
  Polarity getFireflyTxPolarity(const std::string&, const size_t) const;

  std::string dumpFFInfo();

  int ApolloAccess(std::string command_args);
  void AddStream(Level::level level, std::ostream* os);
  void RemoveStream(Level::level level, std::ostream* os);
  int svfPlayer(std::string const & svfFile, std::string const & XVCReg);
  uint32_t ReadRegister(std::string const & reg);
  void WriteRegister(std::string const & reg, uint32_t value);
  void unblockAxi(std::string name="");
  void recreateSMDevice();

private:

  ApolloCMFPGA(const ::swatch::core::AbstractStub& aStub, Site, const ChannelOrigin&);

  bool checkPresence() const final;
  
  // Need to decide what properties we want
  void retrievePropertyValues() override; // IMPLEMENT
  void retrieveMetricValues() override; // IMPLEMENT

  const Site mSite;

  typedef std::map<::swatch::phase2::ChannelID, Polarity, DummyComparator> FireflyPolarityMap_t;
  const FireflyPolarityMap_t mFireflyRxPolarity;
  const FireflyPolarityMap_t mFireflyTxPolarity;
  
  // Template classes defined in Swatch - https://gitlab.cern.ch/cms-cactus/core/swatch/-/blob/master/swatch/action/src/common/Property.cpp#L82
  //Property<std::string>& mCMFPGAType; // IMPLEMENT
  //Property<std::string>& mCMFPGAUID; // IMPLEMENT

  // Template class defined in Swatch - https://gitlab.cern.ch/cms-cactus/core/swatch/-/blob/master/swatch/core/src/common/SimpleMetric.cpp
  SimpleMetric<float>* mPowerMetric; // IMPLEMENT

  static Site getSiteFromId(const std::string &aId);

  static CMFPGAType getCMFPGAType(const std::string &aId);
  static CMFPGAType getCMFPGAType(const Site);

  static size_t getNumberOfPorts(const Site);
  static bool extractRxInversion(const size_t, const ChannelOrigin&);
  static bool extractTxInversion(const size_t, const ChannelOrigin&);

  static boost::optional<::swatch::phase2::ChannelID> extractRxConnection(const size_t i, const ChannelOrigin&);
  static boost::optional<::swatch::phase2::ChannelID> extractTxConnection(const size_t i, const ChannelOrigin&);

  static FireflyPolarityMap_t createFireflyPolarityMapRx(const ChannelOrigin&);
  static FireflyPolarityMap_t createFireflyPolarityMapTx(const ChannelOrigin&);
  static const std::map<CMFPGAType, size_t> kPortCountMap;

  struct CmdIds {
    /* From Serenity
    static const std::string kConfigureClockSynths;
    static const std::string kConfigureXpointSwitch;
    static const std::string kConfigureFireflyRx;
    static const std::string kConfigureFireflyTx;
    static const std::string kResetFireflys;
    static const std::string kPowerCycle;
    */
    static const std::string ApolloAccess;
    static const std::string PowerDown;
    static const std::string PowerUp;
    static const std::string Program;
    static const std::string dumpFFInfo;
  };

  // BUTool::ApolloSMDevice *SMDevice;
  std::unique_ptr<BUTool::ApolloSMDevice> SMDevice;
  std::shared_ptr<ApolloSM> SM;
  std::vector<std::string> arg {"/opt/address_table/connections.xml"};
};

} // apolloherd
} // swatch

#endif //__SWATCH_APOLLOHERD_APOLLOCMFPGA_HPP__
