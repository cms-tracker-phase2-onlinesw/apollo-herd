#ifndef __APOLLO_SWATCH_FIREFLYBASE_HPP__
#define __APOLLO_SWATCH_FIREFLYBASE_HPP__

#include "swatch/phase2/OpticalModule.hpp"

#include <ApolloSM_device/ApolloSM_device.hh>

namespace swatch {
namespace apolloherd {


class FireflyBase : public ::swatch::phase2::OpticalModule {

public:
  FireflyBase(const std::string& aId, const std::string& aAlias, const std::vector<size_t>& aRxIndices, const std::vector<size_t>& aTxIndices);
  virtual ~FireflyBase();

private:
  bool checkPresence() const final;
  
  void retrievePropertyValues() final;
  

  //const std::string mPartStr;
  //Property<std::string>& mPart;
  //Property<std::string>& mSerialNumber;
  //Property<std::string>& mFirmwareVersion;

protected:
  /* Adapt to Apollo
  static std::pair<float, std::string> measurePhysicalQuantity(Element& , const std::string&);

  static std::map<size_t, std::string> measureChannelSetting(Element& , const std::string& , const std::vector<size_t> aChannels);

  static std::map<size_t, bool> measureChannelFlag(Element& , const std::string& , const std::vector<size_t> aChannels, const std::unordered_map<std::string, bool>& aInterpretationMap);
  
  static const std::unordered_map<std::string, bool> kAlarmValueMap;
  static const std::unordered_map<std::string, bool> kEnableValueMap;
  static const std::unordered_map<std::string, bool> kPolarityInvertedValueMap;
  */
 
  //const std::string mElementName;
  
  SimpleMetric<float>& mMetricTemperature;
  SimpleMetric<float>& mMetricOptPow;

  std::shared_ptr<ApolloSM> SM;
  //SimpleMetric<float>& mMetricPeakTemperature;
  
};

} // apolloherd
} // swatch

#endif
