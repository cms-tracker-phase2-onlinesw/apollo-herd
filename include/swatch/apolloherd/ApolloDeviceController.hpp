#ifndef __SWATCH_APOLLOHERD_APOLLODEVICECONTROLLER_HPP__
#define __SWATCH_APOLLOHERD_APOLLODEVICECONTROLLER_HPP__

#include <string>
#include <vector>
#include <algorithm>    // std::copy
#include <iterator>     // std::istream_iterator, std::back_inserter
#include <sstream>      // std::istringstream
#include <inttypes.h>   // int64_t, uint64_t

#include <ApolloSM_device/ApolloSM_device.hh>
#include <ApolloSM/ApolloSM.hh>

#include "RegisterHelper/RegisterHelperIO.hh"

typedef BUTool::RegisterHelperIO::ConvertType ConvertType;

namespace swatch {
namespace apolloherd {

class ApolloDeviceController {
public:
  ApolloDeviceController(const std::string& aURI, const std::string& aAddrTable);

  ~ApolloDeviceController();

  int ApolloAccess(std::string command_args);

  // access ApolloSMDevice's AddOutputStream() method, inherited from BUTextIO via CommandListBase
  void AddStream(Level::level level, std::ostream* os);

  void RemoveStream(Level::level level, std::ostream* os);

  // Access named register read methods of ApolloSM, inherited through IPBusIO class
  std::string GetConvertFormat(std::string const & reg);
  ConvertType GetConvertType(std::string const & reg) const;

  std::vector<std::string> GetRegisterNamesFromTable(std::string const & tableName, int statusLevel=1);

  // ------------------------------------------------------------------------------
  // Following are wrappers for the "ReadConvert" methods of the ApolloSM class,
  // overloaded by the input data type. The "val" input will be modified in place.
  // ------------------------------------------------------------------------------
  void ReadConvert(std::string const & reg, uint64_t & val);
  void ReadConvert(std::string const & reg, int64_t & val);
  void ReadConvert(std::string const & reg, double & val);
  void ReadConvert(std::string const & reg, std::string & val);
  void recreateDevice();


private:
  BUTool::ApolloSMDevice* ptrSMDevice;
  std::shared_ptr<ApolloSM> ptrSM;
  std::vector<std::string> arg {"/opt/address_table/connections.xml"};
};

}   // apolloherd
}   // swatch

#endif
