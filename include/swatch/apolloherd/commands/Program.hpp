#ifndef __SWATCH_APOLLOHERD_COMMANDS_PROGRAM_HPP__
#define __SWATCH_APOLLOHERD_COMMANDS_PROGRAM_HPP__

#include "swatch/action/File.hpp"
#include "emp/swatch/utilities.hpp"
#include "emp/swatch/commands/AbstractProgram.hpp"

#include "swatch/apolloherd/ApolloCMFPGA.hpp"
#include "swatch/apolloherd/helpers/timing.hpp"
#include <BUTool/CommandReturn.hh>

#include <log4cplus/logger.h>
#include <log4cplus/loggingmacros.h>
// #define CPP_ONLY 1
#include "uio_client.hh"


namespace swatch {
namespace apolloherd {
namespace commands {
struct registerCheck
{
    std::string register_name;
    uint16_t value = 0;
    float etime = 0.0;
    timeval timeout;
    bool set_value = false; //If true, set the register to value if it is not read to be value
};
/*
Program class representing the program command for the FPGAs.

Inherits from the virtual base class AbstractProgram, and defines 
the program() method that will handle programming FPGAs.
*/
class Program : public emp::swatch::commands::AbstractProgram {
public: 
  Program(const std::string& aId, swatch::action::ActionableObject& aActionable);
  virtual ~Program();
private:
  void program(const std::string& aProgrammingFilePath);
  int checkRegisterValue(ApolloCMFPGA& ApolloCM, log4cplus::Logger& lLogger, std::ostringstream& oss, registerCheck regCheck);

};

}   // commands
}   // apolloherd
}   // swatch

#endif  // __SWATCH_APOLLOHERD_COMMANDS_PROGRAM_HPP__
