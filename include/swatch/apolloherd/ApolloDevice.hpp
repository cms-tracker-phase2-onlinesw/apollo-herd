#ifndef __SWATCH_APOLLOHERD_APOLLODEVICE_HPP__
#define __SWATCH_APOLLOHERD_APOLLODEVICE_HPP__

#include <sstream>
#include <algorithm>

#include "swatch/phase2/ServiceModule.hpp"
#include "swatch/apolloherd/ApolloDeviceController.hpp"
#include "swatch/apolloherd/ApolloMetric.hpp"

namespace swatch {
namespace apolloherd {

class ApolloDevice : public phase2::ServiceModule {
public:
  ApolloDevice(const swatch::core::AbstractStub& aStub);
  virtual ~ApolloDevice();

  ApolloDeviceController& getController()
  {
    return mController;
  }
  void resetDevice();
private:
  ApolloDeviceController mController;

  void retrieveMetricValues() override;

  std::vector<std::vector<std::string>> mTableInformation;

};

}   // apolloherd
}   // swatch

#endif
