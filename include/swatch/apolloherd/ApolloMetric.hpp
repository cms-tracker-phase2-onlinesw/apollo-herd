#ifndef __SWATCH_APOLLOHERD_APOLLOMETRIC_HPP__
#define __SWATCH_APOLLOHERD_APOLLOMETRIC_HPP__

#include <algorithm>
#include "ApolloSM/ApolloSM.hh"
#include "RegisterHelper/RegisterHelperIO.hh"

typedef BUTool::RegisterHelperIO::ConvertType ConvertType;

namespace swatch {
namespace apolloherd {

class ApolloMetric {
public:
    ApolloMetric(ConvertType convertType, std::string registerName);
    virtual ~ApolloMetric();

    // Getter methods
    std::string getRegisterName()   const;
    std::string getRegisterId()     const;
    ConvertType getConvertType()    const;

    std::string getIdFromRegisterName(std::string registerName);

private:
    ConvertType mConvertType;
    std::string mRegisterName;
    std::string mRegisterId;
};

} // apolloherd
} // swatch


#endif
