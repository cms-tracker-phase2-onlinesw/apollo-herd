# Apollo SWATCH plugin

This repository contains a SWATCH plugin library for the Apollo board. Two classes, `ApolloDevice` and `ApolloCMFPGA`, utilize [BUTool](https://github.com/BU-Tools/BUTool) and its Apollo Service Module control plugin [ApolloSM_plugin](https://github.com/apollo-lhc/ApolloSM_plugin) to interact with the Service Module and Command Module hardware, respectively. In addition, the `ApolloCMFPGA` class builds on top of the `EMPDevice` class located in the [EMP SWATCH](https://gitlab.cern.ch/p2-xware/software/emp-herd) plugin, which provides the CM control class with functionality for controlling the EMP (Extensible, Modular, data Processor) firmware framework running on the Command Module's two main FPGAs - the Kintex and Virtex. 

This plugin is derived from the work of the CMS Phase-2 Tracker Online SW group, whose repositories can be found [here](https://gitlab.cern.ch/cms-tracker-phase2-onlinesw).


## Dependencies

This plugin has several main dependencies:

* [EMP software](https://serenity.web.cern.ch/serenity/emp-fwk/software/)
* [EMP SWATCH plugin](https://gitlab.cern.ch/p2-xware/software/emp-herd/)
* [ApolloTool](https://github.com/apollo-lhc/ApolloTool)
* [SWATCH software](https://gitlab.cern.ch/cms-cactus/core/swatch)

Additional subdependencies may arise from these. Due to compatibility issues with the compiler on the rev.1 Apollo blades, this plugin is no longer meant to be built from source. Instead, the plugin is run in a docker container using the instructions below. 

## Using this plugin with the control application

The HERD control application (implemented in the herd-control-app repository) is an executable that loads SWATCH/HERD plugins, and provides a network interface that allows remote applications to run the commands and FSM transitions procedures declared in those plugins. If run with `Apollo.yml` as the configuration file, the control application will load the SWATCH plugin and create three devices:

* one for the Service Module (named `ApolloSM_device`), and 
* two for the Command Module FPGAs (named `kintex` and `virtex`).

At the moment, the control application can be run with the Apollo plugin only in a docker container:

* The image should always be run using the ```start_apolloherd.sh``` shell script. This script wraps the `docker run` command, makes all UIO devices accessible inside the container, mounts several volumes, and sets permissions.

* As a result of the CI jobs, different Docker images for different architectures will be created (e.g. `arm/v7` on the rev.1 blades and `arm64` on the rev.2 blades). But in addition, a Docker image which should work on different architectures will also be available, and this image can be used to run on different blades. These images can be accessed via the GitLab container registry of the project *either* via `branch-commithash` or `tag` information, and the URLs are listed below: 

  * **Tags:**

    * `gitlab-registry.cern.ch/cms-tracker-phase2-onlinesw/apollo-herd/alma8/herd:vX.Y.Z`

  * **Branches:**

    * `gitlab-registry.cern.ch/cms-tracker-phase2-onlinesw/apollo-herd/alma8/herd:BRANCHNAME-COMMITSHA`

    where `COMMITSHA` is the first 8 characters of the git commit's SHA

The list of built images can be found in the "Container Registry" on Gitlab, using [this](https://gitlab.cern.ch/cms-tracker-phase2-onlinesw/apollo-herd/container_registry/) link. To search for available Docker images to run, the user should search for image versions under 
`cms-tracker-phase2-onlinesw/apollo-herd/alma8/herd`.

The `apollo-herd` Docker image can be run using the `start_apolloherd.sh` script as follows:

```
./path/to/start_apolloherd.sh -e APOLLO_TYPE=Rev2b -d -p 3000:3000 gitlab-registry.cern.ch/cms-tracker-phase2-onlinesw/apollo-herd/alma8/herd:vX.Y.Z
```

An example of running the plugin in a container using the non-default config file located in the `/path/to/apollo-config/` directory on the host machine, mounted as `/config/` in the container:

```
./path/to/start_apolloherd.sh -d -p 3000:3000 -e APOLLO_TYPE=Rev2b -v /path/to/apollo-config:/config gitlab-registry.cern.ch/cms-tracker-phase2-onlinesw/apollo-herd/alma8/herd:vX.Y.Z /config/Apollo.yml 
```

The above command
  
1. runs the container in the background with the `-d` flag
2. maps TCP port `3000` in the container to `3000` on the Docker host
3. mounts the directory `/path/to/apollo-config` on the host to `/config` in the container. In this example, and in practice, the directory should contain one config file, named `Apollo.yml`
4. specifies the appropriate apollo-herd image to run, and, 
5. starts the HERD control app with the appropriate Apollo-specific config file, `Apollo.yml`

Once the above command finishes executing, the user can check the container status using the following `docker` commands:

```
# Show information about the running container
docker ps

# Get logs of the running container (works if only one container is running)
docker ps -q | xargs docker logs -t

# If more than one containers are running, user can call docker logs manually
docker logs <containerID>
```

## Running the Container Interactively

For debugging the image, you should run `start_apolloherd.sh` with the flag combination `-it --entrypoint bin/bash` in order to run the container interactively without the HERD control application starting up. This can be achieved by the following command:

```
./start_apolloherd.sh -it -p 3000:3000 -e APOLLO_TYPE=Rev2b --entrypoint bin/bash gitlab-registry.cern.ch/cms-tracker-phase2-onlinesw/apollo-herd/alma8/herd:vX.Y.Z
```

Once inside the container, you can start the HERD plugin by running:

```
# Set up the environment for HERD plugin
source entrypoint_env.sh

# Start the server on port 3000
herd herd.yml
```

## Developing and building directly on the board

If you need to test some features directly on the board, bypassing the CI dev chain, it is possible to achieve in two similar ways:
 - Use `devcontainer` feature of VSCode with container configured in `.devcontainer/devcontainer.json`

As a base image we will use the emp-herd one, which includes some upstream developments that we want to build on:

```json
"image": "gitlab-registry.cern.ch/p2-xware/software/emp-herd/alma8/herd-devel:program-pass-tgz-if-requested-e22bb66b"
```

But usually one should use the image from `.gitlab-ci.ymal`.

 - Using `docker/podman` directly, without VSCode UI

Star the container with:

```bash
docker run -it --privileged --net=host -v /fw:/fw   -v /dev:/dev   -v /opt/address_table:/opt/address_table  -v /path/to/sourcecode/apollo-herd/etc:/etc/Apollo/  -v /proc:/proc   -v /tmp:/tmp -v ./:/APOLLO-HERD  --entrypoint bash   -e APOLLO_TYPE=Rev2b   -e CONFIG_EXT=Optics gitlab-registry.cern.ch/p2-xware/software/emp-herd/alma8/herd-devel:program-pass-tgz-if-requested-e22bb66b
```

Inside the docker container: adapt if needed and install dependencies. Set `set +e` option that won't crush the container on error (only used on the dev step)

```bash
source ci/install_dependencies.sh
set +e
```
Develop, build and install your code in the image:

```bash
mkdir build; cd build
cmake ..
make -j4
make install 
cd ..
source ci/entrypoint_env.sh
source env.sh
```
Test the app:

```bash
herd Apollo.yml
```
