#!/bin/bash

set -e
set -x

UIOUHAL_VERSION="v2.0.0"
APOLLOTOOL_VERSION="v3.2.1"
BOOTUP_DAEMON_VERSION="makefile/pybind11"

if [ "$1" != "app" ]; then

    # 1) Install dependencies of uHAL and UIOuHAL
    dnf -y install git \
        readline-devel \
        zlib-devel \
        rpm-build \
        git-core \
        boost-devel \
        pugixml-devel \
        python38-devel \
        gdb \

    dnf clean all

    # # Debug statement to list all installed packages
    # echo "Listing all installed packages after first dnf install..."
    # dnf list installed

    # Debug statement to check for python3.8-config
    echo "Checking for /usr/bin/python3.8-config..."
    ls -l /usr/bin/python3.8-config

    # Create symlink for python3-config if python3.8-config exists
    if [ -f /usr/bin/python3.8-config ]; then
        ln -s /usr/bin/python3.8-config /usr/bin/python3-config
    else
        echo "Error: /usr/bin/python3.8-config not found."
        exit 1
    fi

    # 2) uHAL v2.8.x is already installed in the parent emp-herd image
    # Here, set the UHAL_VER environment variables for UIOuHAL & BUTool use
    export UHAL_VER_MAJOR=2
    export UHAL_VER_MINOR=8

    # 3) building UIOuHAL
    export CACTUS_ROOT=/opt/cactus/
    git clone --depth 1 --branch ${UIOUHAL_VERSION} https://github.com/BU-Tools/UIOuHAL.git

    # build
    cd UIOuHAL/
    make
    mkdir -p /opt/UIOuHAL
    cp -r lib /opt/UIOuHAL
    cp -r include /opt/UIOuHAL
    cd ..
    rm -rf UIOuHAL

    # 4) install dependencies of BUTool         #boost-python3 not found atm
    dnf -y install boost-chrono \
        boost-regex \
        boost-thread \
        boost-filesystem \
        boost-system \
        boost-devel \
        boost-program-options \
        systemd-devel \

    dnf clean all

    # # Debug statement to list all installed packages
    # echo "Listing all installed packages after second dnf install..."
    # dnf list installed

    # Additional steps to ensure correct Python headers are found
    export CPLUS_INCLUDE_PATH=/usr/include/python3.8

    # 5) build BUTool from ApolloTool meta repository
    git clone --depth 1 --branch ${APOLLOTOOL_VERSION} https://github.com/apollo-lhc/ApolloTool.git
    cd ApolloTool
    
    make init

    # Remove possible artifacts from previous builds
    make clean

    # Build ApolloTool, skip the linting step by setting LINTER=:
    LINTER=: make local -j$(nproc) RUNTIME_LDPATH=/opt/BUTool COMPILETIME_ROOT=--sysroot=/
    LINTER=: make install RUNTIME_LDPATH=/opt/BUTool COMPILETIME_ROOT=--sysroot=/ INSTALL_PATH=/opt/BUTool
    cd ..
    rm -rf ApolloTool

    # uio daemon plugin dependencies 
    # this repo is needed for protobuf
    dnf config-manager --set-enabled powertools
   
    # Create the repo file with all EDF sw, use it to intall the uio-daemon plugin
    echo "[edf_tools]" > /etc/yum.repos.d/edf_tools.repo
    echo "name=EDF SW tools" >> /etc/yum.repos.d/edf_tools.repo
    echo "baseurl=http://ohm.bu.edu/repo/AlmaLinux/8/\$basearch/" >> /etc/yum.repos.d/edf_tools.repo
    echo "enabled=1" >> /etc/yum.repos.d/edf_tools.repo
    echo "gpgcheck=0" >> /etc/yum.repos.d/edf_tools.repo

    echo "Repository file /etc/yum.repos.d/edf_tools.repo has been created."

    dnf makecache
    dnf install -y pugixml-devel protobuf-devel
    python3.8 -m pip install pybind11

    dnf install -y UIODaemonPlugin


    # Extra install packages
    dnf -y install log4cplus /

    dnf clean all

    # Debug statement to list all installed packages
    # echo "Listing all installed packages after final dnf install..."
    # dnf list installed

else
    dnf config-manager --set-enabled powertools
    dnf makecache
    dnf install -y protobuf-devel

fi
