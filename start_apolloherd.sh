#!/usr/bin/env bash

# ---------------------------------------------------------------------
# Wrapper script for 'docker run'
#
#   Automatically adds all flags required to communicate with the 
#   hardware access software on the Apollo boards. All user-supplied 
#   flags and arguments are added on the command line and passed to 
#   this script after the mandatory flags included here
#
# Usage:
#
#   start_apolloherd.sh <flags> <apollo-herd image URL or ID>
#
# Example:
#
#   start_apolloherd.sh -it -v /local/path:/path/inside/container imageID_or_imageURL
#
# The above example will run the container interactively and mount some
# directory on the host to the specified path inside the container
#
# Credit: Tom Williams, RAL
#
# ---------------------------------------------------------------------

set -e

# gather all the UIO devices, use --device flag to make accessible in container
#UIOs=$(ls -1 /dev/uio* | awk '{print " --device="$1":"$1}' | xargs)

# build array with command and flags
declare -a DOCKER_RUN_CMD=(docker run)
# pass all UIO devices to container
#DOCKER_RUN_CMD+=($UIOs)
DOCKER_RUN_CMD+=(-v /dev:/dev)

# mount address tables
DOCKER_RUN_CMD+=(-v /opt/address_table:/opt/address_table)
DOCKER_RUN_CMD+=(-v /fw:/fw)
ConfigDirectory=$(realpath etc)
DOCKER_RUN_CMD+=(-v $ConfigDirectory:/etc/Apollo/)

# mount proc fs
DOCKER_RUN_CMD+=(-v /proc:/proc)
# mount folder where uio daemon socket lives
DOCKER_RUN_CMD+=(-v /var/run/uio_daemon:/var/run/uio_daemon)


# run as privileged (for now)
DOCKER_RUN_CMD+=(--privileged)

# Function to parse and add environment variables from command line
add_env_vars() {
  local required_vars=("APOLLO_TYPE")  # Only required vars
  local optional_vars=("CONFIG_EXT")
  declare -A env_vars_passed
  declare -A env_vars_defaults=(["CONFIG_EXT"]="")

  while (( "$#" )); do
    if [[ $1 == -e ]]; then
      env_var_name=$(echo "$2" | cut -d'=' -f1)
      env_var_value=$(echo "$2" | cut -d'=' -f2-)
      if [[ " ${required_vars[@]} ${optional_vars[@]} " =~ " ${env_var_name} " ]]; then
        DOCKER_RUN_CMD+=(-e "$2")
        env_vars_passed["$env_var_name"]=1
      fi
      shift 2
    else
      shift
    fi
  done

  # Check if all required environment variables were provided
  for var in "${required_vars[@]}"; do
    if [[ -z "${env_vars_passed[$var]}" ]]; then
      echo "Error: Missing required environment variable: $var"
      exit 1
    fi
  done

  # Set default values for optional environment variables if they are not provided
  for var in "${optional_vars[@]}"; do
    if [[ -z "${env_vars_passed[$var]}" ]]; then
      DOCKER_RUN_CMD+=(-e "${var}=${env_vars_defaults[$var]}")
    fi
  done
}

# Add environment variables passed from the command line
add_env_vars "$@"

"${DOCKER_RUN_CMD[@]}" "$@"
