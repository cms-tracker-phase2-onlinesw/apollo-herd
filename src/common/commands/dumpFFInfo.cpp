#include "swatch/apolloherd/commands/dumpFFInfo.hpp"

namespace swatch {
namespace apolloherd {
namespace commands {

dumpFFInfo::dumpFFInfo(const std::string& aId, action::ActionableObject& aActionable) :
  Command(aId, "dumpFFInfo", "Print FF information compiled by the ApolloCMFPGA", aActionable, std::string())
{
  registerParameter<std::string>("Dummy", "Dummy");
}

dumpFFInfo::~dumpFFInfo()
{
}

action::Command::State dumpFFInfo::code(const core::ParameterSet& aParams) 
{
  // get the ApolloCMFPGA
  ApolloCMFPGA& ApolloCM = getActionable<ApolloCMFPGA>();

  std::ostringstream oss;
  ApolloCM.AddStream(Level::INFO, &oss);

  // FF Info Dump
  setProgress(0.5, "Getting FF Info Dump");
  
  std::string dummy = aParams.get<std::string>("Dummy");
  std::string result = ApolloCM.dumpFFInfo();

  setProgress(0.99, "Printing Info Dump to Screen");

  // return result to Shep
  setStatusMsg("test");
  setStatusMsg(result);
  setResult(oss.str());

  return State::kDone;
  ApolloCM.RemoveStream(Level::INFO, &oss);
}

} // commands
} // apolloherd
} // swatch