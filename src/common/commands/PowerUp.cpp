#include "swatch/apolloherd/commands/PowerUp.hpp"

namespace swatch {
namespace apolloherd {
namespace commands {

PowerUp::PowerUp(const std::string& aId, action::ActionableObject& aActionable) :
  Command(aId, "PowerUp", "Power up the command module", aActionable, std::string())
{
  registerParameter<std::string>("wait (s)", "5");
}

PowerUp::~PowerUp()
{
}

action::Command::State PowerUp::code(const core::ParameterSet& aParams) 
{
  // Get the ApolloCMFPGA
  ApolloCMFPGA& ApolloCM = getActionable<ApolloCMFPGA>();

  // Create a stringstream to capture output
  std::ostringstream oss;
  ApolloCM.AddStream(Level::INFO, &oss);

  // Get the CM ID
  std::string CMID;
  // for now we have only CM1 which brings up both fpga
  // switch (ApolloCM.getSite()) {
  //   case Site::F1:
  //     CMID = "1";
  //     break;
  //   case Site::F2:
  //     CMID = "2";
  //     break;
  // }
  CMID = "1";

  // Get the wait time parameter
  std::string wait = aParams.get<std::string>("wait (s)");

  // Attempt to cast 'wait' to a float to validate it
  try {
    float wait_time = std::stof(wait);
    if (wait_time <= 0.0f) {
      ApolloCM.RemoveStream(Level::INFO, &oss);
      throw core::RuntimeError("Invalid wait time: must be a positive number.");
    }
  } catch (const std::invalid_argument& e) {
    ApolloCM.RemoveStream(Level::INFO, &oss);
    throw core::RuntimeError("Invalid wait time: must be a numeric value.");
  } catch (const std::out_of_range& e) {
    ApolloCM.RemoveStream(Level::INFO, &oss);
    throw core::RuntimeError("Invalid wait time: value is out of range.");
  }

  // Power up CM
  setProgress(0.5, "Powering up CM_" + CMID);
  std::string command_and_args = "cmpwrup " + CMID + " " + wait;
  int result = ApolloCM.ApolloAccess(command_and_args);

  if (result == CommandReturn::status::BAD_ARGS){
    ApolloCM.RemoveStream(Level::INFO, &oss);
    throw core::RuntimeError("Bad arguments");
  }

  // Check the output message for "failed"
  std::string output_message = oss.str();
  if (output_message.find("failed") != std::string::npos) {
    ApolloCM.RemoveStream(Level::INFO, &oss);
    throw core::RuntimeError("Power up took longer than the given time");
  }

  // Return cmpwrup result to Shep
  setResult(output_message);
  ApolloCM.RemoveStream(Level::INFO, &oss);

  return State::kDone;
}

} // namespace commands
} // namespace apolloherd
} // namespace swatch
