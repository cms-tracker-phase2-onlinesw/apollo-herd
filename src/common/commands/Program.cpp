#include "swatch/apolloherd/commands/Program.hpp"
#include "swatch/apolloherd/ApolloDevice.hpp"
#include "swatch/apolloherd/ApolloDeviceController.hpp"

#include <yaml-cpp/yaml.h>
#include <unistd.h>

namespace swatch {
namespace apolloherd {
namespace commands {

using namespace ::swatch;

Program::Program(const std::string& aId, action::ActionableObject& aActionable) :
  AbstractProgram(aId, aActionable, "")
{
}

Program::~Program() 
{
}

void Program::program(const std::string& aProgrammingFilePath)
/*
Function called by the base class AbstractProgram to program the FPGA.
aProgrammingFilePath argument should be point to the .svf file we want to use.
*/
{
  // Define logger
  log4cplus::Logger lLogger(log4cplus::Logger::getInstance("swatch.apolloherd.Program"));
  
  /* Path to the UNIX socket where the uio-daemon server is listening. */
  std::string socket_path = "/var/run/uio_daemon/uio_socket";

  UIOStatus status;

  UIOClient client(socket_path);
  /* Step 1: Get UID from server. */
  status = client.Init();
  if (status != UIOStatus::OK) {
      LOG4CPLUS_DEBUG(lLogger, "Failed to obtain a UID for the client");
      throw core::RuntimeError("Failed to obtain a UID for the client");
  }

  LOG4CPLUS_DEBUG(lLogger, "Client ID obtained from server: 0x" << client.GetUID());
  
  /* Step 2: Load overlays and program an FPGA. */
  LOG4CPLUS_DEBUG(lLogger, "Loading overlays");
  LOG4CPLUS_DEBUG(lLogger, "Using programming file: " << aProgrammingFilePath << "\n");


  status = client.SendLoadTarMessage(aProgrammingFilePath);
  if (status != UIOStatus::OK) {
      std::string error = "Failed to load tar for the client, status: " + std::to_string(static_cast<int>(status));
      LOG4CPLUS_DEBUG(lLogger,  error);
      throw core::RuntimeError( error);
  }

  // No need to power up, because it is done by previous command, although it might change in the future
  // status = client.SendCMPowerUpMessage();
  // if (status != UIOStatus::OK) {
  //     std::string error = "Failed to power up CM for the client, status: " + std::to_string(static_cast<int>(status));
  //     LOG4CPLUS_DEBUG(lLogger,  error);
  //     throw core::RuntimeError( error);
  // }

  setProgress(0.01, "Programming an FPGA with UIOClient");
  LOG4CPLUS_DEBUG(lLogger, "Using programming file: " << aProgrammingFilePath << "\n");
  
  status = client.SendProgramFPGAMessage();
  if (status != UIOStatus::OK) {
      std::string error = "Failed to program FPGA, status: " + std::to_string(static_cast<int>(status));
      LOG4CPLUS_DEBUG(lLogger,  error);
      throw core::RuntimeError( error);
  }

  std::map<std::string, std::string> deviceMap;

  status = client.GetUIODevices(deviceMap);
  if (status != UIOStatus::OK) {
      std::string error = "Failed to get UIO Devices, status: " + std::to_string(static_cast<int>(status));
      LOG4CPLUS_DEBUG(lLogger,  error);
      throw core::RuntimeError( error);
  }

  for (auto const& [key, val] : deviceMap) {
      LOG4CPLUS_DEBUG(lLogger, "Device: " << key << " Path: " << val);
  }


  getActionable<ApolloCMFPGA>().recreateSMDevice();
  setProgress(0.93, "Re-initialised ApolloSM device of ApolloCMFPGA  with re-written address tables");

  // Get an ApolloCMFPGA, it should be instantiated after the daemon wrote address tables 
  ApolloCMFPGA& ApolloCM = getActionable<ApolloCMFPGA>();

  // Give stringstream to ApolloSMDevice
  std::ostringstream oss;
  ApolloCM.AddStream(Level::INFO, &oss);

  

  // Get the CM ID: Kintex or Virtex
  std::string CMID;
  switch (ApolloCM.getSite()) {
    case Site::F1:
      CMID = "1";
      break;
    case Site::F2:
      CMID = "2";
      break;
  }

  // Set-up Yaml Configuration File and Nodes
  YAML::Node config;
  try{
    config = YAML::LoadFile("/etc/Apollo/Program.yml");
  } catch(const YAML::BadFile& e){
    ApolloCM.RemoveStream(Level::INFO, &oss);
    throw core::RuntimeError("Bad /etc/Apollo/Program.yml file!");
  }

  YAML::Node NodeSVF = config["SVF"];
  YAML::Node NodeC2C = config["C2C"]["F"+CMID];
  YAML::Node NodeCM = config["CM"]["F"+CMID];
  YAML::Node NodeCLK = config["CLK"];

  setProgress(0.95, "Checking C2C Links");

  // Setting up timing variables
  struct timeval timeout;
  timeout.tv_sec  = 0;
  timeout.tv_usec = 100000;

  struct timeval increased_timeout;
  increased_timeout.tv_sec  = 0;
  increased_timeout.tv_usec = 1000000;

  // Checking CM.CM_CMID.C2C_1.COUNTERS.PHYLANE_STATE
  std::string register_name = NodeC2C["Phylane_1"].as<std::string>();

  registerCheck regCheck = {register_name, 4, 5.0, timeout, false};
  this->checkRegisterValue(ApolloCM, lLogger, oss, regCheck);


  // Checking CM.CM_CMID.C2C_2.COUNTERS.PHYLANE_STATE
  register_name = NodeC2C["Phylane_2"].as<std::string>();
  regCheck = {register_name, 4, 1.0, timeout, false};
  this->checkRegisterValue(ApolloCM, lLogger, oss, regCheck);

  setProgress(0.96, "Checking C2C AXI registers");

  // Checking C2CCMID_AXI_FW.STATUS and C2CCMID_AXILITE_FW.STATUS
  std::string access_C2C_Axi = NodeC2C["AXI"].as<std::string>();
  regCheck = {access_C2C_Axi, 0, 1.0, timeout, false};
  this->checkRegisterValue(ApolloCM, lLogger, oss, regCheck);

  setProgress(0.97, "Checking C2C AXILITE registers");
  std::string access_C2C_Axilite = NodeC2C["AXILITE"].as<std::string>();
  regCheck = {access_C2C_Axilite, 0, 1.0, timeout, false};
  this->checkRegisterValue(ApolloCM, lLogger, oss, regCheck);
  
  setProgress(0.98, "Checking and setting TCDS path registers");
  // Set clock sel and source to 0 so that tcds2 can be sent to the CM
  std::string clk_sel = NodeCLK["SEL"].as<std::string>();
  regCheck = {clk_sel, 0, 1.0, timeout, true};
  this->checkRegisterValue(ApolloCM, lLogger, oss, regCheck);

  std::string clk_source = NodeCLK["TTC"].as<std::string>();
  regCheck = {clk_source, 0, 1.0, timeout, true};
  this->checkRegisterValue(ApolloCM, lLogger, oss, regCheck);

  setProgress(0.99, "Checking CM registers");
  //Attempt CM Register Read
  std::string CM_Read = NodeCM["Register"].as<std::string>();
  regCheck = {CM_Read, 1, 5.0, increased_timeout, false};
  this->checkRegisterValue(ApolloCM, lLogger, oss, regCheck);
  
  ApolloCM.RemoveStream(Level::INFO, &oss);
}
//
int Program::checkRegisterValue(ApolloCMFPGA& ApolloCM, log4cplus::Logger& lLogger, std::ostringstream& oss, registerCheck regCheck)
{
  bool read_flag = false;
  float elapsed_time = 0.0;
  struct timeval dummy_time;
  while(elapsed_time < regCheck.etime){
    if(ApolloCM.ReadRegister(regCheck.register_name) == regCheck.value){
      LOG4CPLUS_DEBUG_FMT(lLogger, "Register %s reads 0x%x - Success!\n" , regCheck.register_name.c_str(), ApolloCM.ReadRegister(regCheck.register_name));
      read_flag = true;
      break;
    }
    else{
      if(regCheck.set_value == true){
        LOG4CPLUS_DEBUG_FMT(lLogger, "Register %s reads 0x%x - Attempting to set to 0x%x\n" , regCheck.register_name.c_str(), ApolloCM.ReadRegister(regCheck.register_name), regCheck.value);
        ApolloCM.WriteRegister(regCheck.register_name, regCheck.value);
      }

      LOG4CPLUS_DEBUG_FMT(lLogger, "Register %s reads 0x%x - Attempting read again in %ld microseconds\n" , regCheck.register_name.c_str(), ApolloCM.ReadRegister(regCheck.register_name),  regCheck.timeout.tv_usec);
      struct timeval dummy_time =  regCheck.timeout;
      int ret = select(1, NULL, NULL, NULL, &dummy_time); //Sleep timeout.tv_usec microseconds
      if(ret == -1){
        ApolloCM.RemoveStream(Level::INFO, &oss);
        throw core::RuntimeError("Error thrown by timing function select() in Program.cpp");
      }
      elapsed_time += timeval2float(regCheck.timeout); //Update time elapsed
      LOG4CPLUS_DEBUG_FMT(lLogger, "Time Elapsed - %.6f\n" , elapsed_time);
      if(regCheck.register_name.find("AXI") != std::string::npos) {
        ApolloCM.unblockAxi();
      }
    }
  }
  if(read_flag == false){
    ApolloCM.RemoveStream(Level::INFO, &oss);
    throw core::RuntimeError("Unable to read correct value from register in 20 seconds in Program.cpp - Register: " + regCheck.register_name + " Value: " + std::to_string(ApolloCM.ReadRegister(regCheck.register_name)));
  }
  return 0;
}



}   // commands
}   // apolloherd
}   // swatch
