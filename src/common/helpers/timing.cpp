#include "swatch/apolloherd/helpers/timing.hpp"

double diff_time(struct timeval t0, struct timeval t1){

  struct timeval result;

  if(t1.tv_usec > t0.tv_usec){ //make usec difference positive
    int carry = (t1.tv_usec - t0.tv_usec) / 1000000 + 1;
    t1.tv_usec -= carry * 1000000;
    t1.tv_sec += carry;
  }
  if(t0.tv_usec - t1.tv_usec > 999999){ //make usec difference between 0-999999
    int overlap = (t1.tv_usec - t0.tv_usec) / 1000000;
    t1.tv_usec += overlap*1000000;
    t1.tv_sec -= overlap;
  }

  result.tv_sec = t0.tv_sec - t1.tv_sec;
  result.tv_usec = t0.tv_usec - t1.tv_usec;
  return result.tv_sec + result.tv_usec / 1000000.0;
}

double diff_time(struct timespec t0, struct timespec t1){

  struct timespec result;

  if(t1.tv_nsec > t0.tv_nsec){ //make nsec difference positive
    int carry = (t1.tv_nsec - t0.tv_nsec) / 1000000000 + 1;
    t1.tv_nsec -= carry * 1000000000;
    t1.tv_sec += carry;
  }

  if(t0.tv_nsec - t1.tv_nsec > 999999999){ //make usec difference between 0-999999999
    int overlap = (t1.tv_nsec - t0.tv_nsec) / 1000000000;
    t1.tv_nsec += overlap*1000000000;
    t1.tv_sec -= overlap;
  }

  result.tv_sec = t0.tv_sec - t1.tv_sec;
  result.tv_nsec = t0.tv_nsec - t1.tv_nsec;

  return result.tv_nsec / 1000000000.0;
}

double diff_time(time_t t0, time_t t1){
  return (double)(t0-t1);
}

double timeval2float(timeval t0){
  return (double)(t0.tv_sec + t0.tv_usec / 1000000.0);
}