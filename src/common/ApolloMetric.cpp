#include <cctype>
#include <algorithm>

#include "swatch/apolloherd/ApolloMetric.hpp"

namespace swatch {
namespace apolloherd {

/*
 * ApolloMetric class that wrappes a metric for a single register.
 * It holds the following information for a register:
 *    - Register ID (unique)
 *    - Register name
 *    - Data type
 */
ApolloMetric::ApolloMetric(ConvertType convertType, std::string registerName)
  : mConvertType(convertType),
    mRegisterName(registerName)
{
    mRegisterId = getIdFromRegisterName(registerName);
}

ApolloMetric::~ApolloMetric()
{
}

// Getter methods for private members
std::string ApolloMetric::getRegisterId()     const { return mRegisterId;   }
std::string ApolloMetric::getRegisterName()   const { return mRegisterName; }
ConvertType ApolloMetric::getConvertType()    const { return mConvertType;  }

std::string ApolloMetric::getIdFromRegisterName(std::string registerName)
{
    /*
     * Returns a unique ID for the given register, by removing the non-alphanumeric characters from it
     */
    registerName.erase(std::remove_if(registerName.begin(), registerName.end(),
    []( auto const& c ) -> bool { return !std::isalnum(c); } ), registerName.end() );

    return registerName;   
}

} // apolloherd
} // swatch

