#include "swatch/apolloherd/ApolloDeviceController.hpp"
#include <uhal/ConnectionManager.hpp>

namespace swatch {
namespace apolloherd {

ApolloDeviceController::ApolloDeviceController(const std::string& aURI, const std::string& aAddrTable)
{
  ptrSM = std::make_shared<ApolloSM>(arg);
  ptrSMDevice = new BUTool::ApolloSMDevice(ptrSM);
}

ApolloDeviceController::~ApolloDeviceController() 
{
  if (NULL != ptrSMDevice) {
      delete ptrSMDevice;
  }
}

void ApolloDeviceController::recreateDevice()
{
  // clear uHal cashed address tables before creating a new device
  uhal::ConnectionManager::clearAddressFileCache();
  // Recreate SM
  ptrSM.reset(new ApolloSM(arg));
  // Recreate SMDevice
  if (NULL != ptrSMDevice) {
      delete ptrSMDevice;
  }
  ptrSMDevice = new BUTool::ApolloSMDevice(ptrSM);

}

void ApolloDeviceController::AddStream(Level::level level, std::ostream* os) {
  // calls ApolloSMDevice's AddOutputStrea() method inherited from BUTextIO through CommandListBase
  ptrSMDevice->AddOutputStream(level, os);
}

void ApolloDeviceController::RemoveStream(Level::level level, std::ostream* os) {
  ptrSMDevice->RemoveOutputStream(level, os);
}

std::string ApolloDeviceController::GetConvertFormat(std::string const & reg) {
  // Retrieve the format of the data in this named register
  return ptrSM->GetConvertFormat(reg);
}

ConvertType ApolloDeviceController::GetConvertType(std::string const & reg) const {
  return ptrSM->GetConvertType(reg);
}

std::vector<std::string> ApolloDeviceController::GetRegisterNamesFromTable(std::string const & tableName, int statusLevel) {
  // Get list of register names from a specified table 
  return ptrSM->GetRegisterNamesFromTable(tableName, statusLevel);
}

/*
Following are wrappers for the "ReadConvert" methods of the ApolloSM class,
overloaded by the input data type. The "val" argument is passed as a reference and 
will be updated in place once the read transaction is done.
*/
void ApolloDeviceController::ReadConvert(std::string const & reg, uint64_t & val) {
  ptrSM->ReadConvert(reg, val);
}

void ApolloDeviceController::ReadConvert(std::string const & reg, int64_t & val) {
  // Create a copy of the 32-bit integer as a 64-bit integer, and pass that to ApolloSM::ReadConvert()
  // This inner conversion is needed because SimpleMetric does not support 64-bit integers
  ptrSM->ReadConvert(reg, val);
}

void ApolloDeviceController::ReadConvert(std::string const & reg, double & val) {
  ptrSM->ReadConvert(reg, val);
}

void ApolloDeviceController::ReadConvert(std::string const & reg, std::string & val) {
  ptrSM->ReadConvert(reg, val);
}

int ApolloDeviceController::ApolloAccess(std::string command_args)
{
  // split command_args into constituent parts
  // https://stackoverflow.com/a/20114104
  std::istringstream iss(command_args);
  std::vector<std::string> CommandArgs;
  std::copy(std::istream_iterator<std::string>(iss),
            std::istream_iterator<std::string>(),
            std::back_inserter(CommandArgs));
  return ptrSMDevice->EvaluateCommand(CommandArgs);
}

}   // apolloherd
}   // swatch
