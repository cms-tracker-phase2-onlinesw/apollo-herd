
#include "swatch/apolloherd/definitions.hpp"

namespace swatch {
namespace apolloherd {

std::string SiteToString(const Site& site) {
    switch (site) {
    case Site::F1:
        return "F1";
    case Site::F2:
        return "F2";
    default:
        return "Unknown"; // Provide a default return value
    }
}

std::string CMFPGATypeToString(const CMFPGAType& cType) {
    switch (cType) {
    case CMFPGAType::VU13p_F1:
        return "VU13p_F1";
    case CMFPGAType::VU13p_F2:
        return "VU13p_F2";
    default:
        return "Unknown CMFPGA Type";
    }
}

std::string CMBoardTypeToString(const CMBoardType& cType) {
    switch (cType) {
    case CMBoardType::Rev2b:
        return "Rev2b";
    default:
        return "Unknown Board Type";
    }
}

}
}