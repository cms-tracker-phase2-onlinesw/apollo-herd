#include "swatch/apolloherd/FireflyTx.hpp"

#include "swatch/core/MetricConditions.hpp"


namespace swatch {
namespace apolloherd {

using namespace ::swatch;

const std::vector<size_t> FireflyTx::kChannels{ 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11 };

FireflyTx::FireflyTx(const std::string& aId) :
  FireflyTx(aId, aId)
{
}

FireflyTx::FireflyTx(const std::string& aId, const std::string& aAlias) :
  FireflyBase(aId, aAlias, { }, kChannels),
  FF_name(aId)
{
  /* Adapt to Apollo 
  for (auto* channel: getOutputs()) {
    mMetricLOL[channel] = &channel->registerMetric<bool>("LOL", "CDR lock lost (LOL)", ::swatch::core::EqualCondition<bool>(true));
    mMetricLaserFault[channel] = &channel->registerMetric<bool>("laserFault", "Laser fault", ::swatch::core::EqualCondition<bool>(true));

    // Declare that this monitorable object will update each of the above per-channel metrics
    channel->delegateMetricUpdate(mMetricLOL.at(channel)->getId(), *this);
    channel->delegateMetricUpdate(mMetricLaserFault.at(channel)->getId(), *this);
  }
  */
}


FireflyTx::~FireflyTx()
{
}


void FireflyTx::retrieveMetricValues()
{
  /* Adapt to Apollo 
  std::unique_ptr<Smash> lSmash(startSmashSession());
  auto& lFirefly(*lSmash->GetElement(mElementName));

  setMetric(mMetricPeakTemperature, measurePhysicalQuantity(lFirefly, "Peak Temperature").first);

  const auto lLOL = measureChannelFlag(lFirefly, "CDR LOL alarms", { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11 }, kAlarmValueMap);
  const auto lLaserFault = measureChannelFlag(lFirefly, "Laser fault alarms", { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11 }, kAlarmValueMap);

  for (auto* channel: getOutputs()) {
    setMetric(*mMetricLOL.at(channel), lLOL.at(channel->getIndex()));
    setMetric(*mMetricLaserFault.at(channel), lLaserFault.at(channel->getIndex()));
  }
  */
  setMetric(mMetricTemperature, static_cast<float>(SM->ReadRegister("PL_MEM_CM." + FF_name + ".Temp_C")));
  setMetric(mMetricOptPow, static_cast<float>(SM->ReadRegister("PL_MEM_CM." + FF_name + ".OptPow")));
}


void FireflyTx::retrieveOutputPropertyValues(Channel& aChannel)
{
  /* Adapt to Apollo 
  std::unique_ptr<Smash> lSmash(startSmashSession());
  auto& lFirefly(*lSmash->GetElement(mElementName));

  const auto lEnabled1 = measureChannelFlag(lFirefly, "Channel", kChannels, kEnableValueMap);
  const auto lEnabled2 = measureChannelFlag(lFirefly, "Output", kChannels, kEnableValueMap);
  const auto lEqualization = measureChannelSetting(lFirefly, "Equalization", kChannels);
  const auto lInvPolarity = measureChannelFlag(lFirefly, "Polarity", kChannels, kPolarityInvertedValueMap);
  const auto lSquelch = measureChannelFlag(lFirefly, "Squelch", kChannels, kEnableValueMap);
  const auto lCDR = measureChannelFlag(lFirefly, "CDR", kChannels, kEnableValueMap);

  if (lEnabled1.at(aChannel.getIndex()) and lEnabled2.at(aChannel.getIndex()))
    set<std::string>(*mPropertyState.at(&aChannel), "Enabled");
  else if (lEnabled1.at(aChannel.getIndex()) or lEnabled2.at(aChannel.getIndex()))
    set<std::string>(*mPropertyState.at(&aChannel), "Mixed");
  else
    set<std::string>(*mPropertyState.at(&aChannel), "Disabled");

  set<std::string>(*mPropertyEqualization.at(&aChannel), lEqualization.at(aChannel.getIndex()));
  set<std::string>(*mPropertyPolarity.at(&aChannel), std::string(lInvPolarity.at(aChannel.getIndex()) ? "Inverted" : "Normal"));
  set<std::string>(*mPropertySquelch.at(&aChannel), lSquelch.at(aChannel.getIndex()) ? "Enabled" : "Disabled");
  set<std::string>(*mPropertyCDR.at(&aChannel), lCDR.at(aChannel.getIndex()) ? "Enabled" : "Disabled");
  */
}


void FireflyTx::retrieveOutputMetricValues(Channel& aChannel)
{
  // Monitoring data for output channels updated by parent instead (since called delegateUpdateMetrics in CTOR)
}




} // apolloherd
} // swatch
