
#include "swatch/apolloherd/utilities.hpp"

#include <chrono>
#include <cstdlib>
#include <cstring>
#include <algorithm>
#include <cstddef>
#include <iostream>
#include <fstream>
#include <list>
#include <mutex>
#include <thread>

#include <boost/filesystem.hpp> // or #include "boost/filesystem.hpp"

#include <log4cplus/loggingmacros.h>

#include <yaml-cpp/yaml.h>

namespace swatch {
namespace apolloherd {

const std::unordered_map<std::string, CMFPGAType> kCMFPGAEnvValueMap{
  {"VU13p_F1", CMFPGAType::VU13p_F1},
  {"VU13p_F2", CMFPGAType::VU13p_F2},
};

const std::map<CMFPGAType, std::string> kCMFPGASpecPaths {
  { CMFPGAType::VU13p_F1, "/etc/Apollo/PinInfo_Rev2b_VU13p_F1.yaml" },
  { CMFPGAType::VU13p_F2, "/etc/Apollo/PinInfo_Rev2b_VU13p_F2.yaml" },
};

boost::optional<CMFPGAType> getApolloCMFPGAType(const Site aSite)
{
  const std::string lEnvVarName(aSite == Site::F1 ? "CMFPGA_F1" : "CMFPGA_F2");
  if (const char* lEnvVarCStr = std::getenv(lEnvVarName.c_str())) {
    // Check if string is broken or empty - Return empty CMFPGAType
    if (strcmp(lEnvVarCStr, "") == 0 || strcmp(lEnvVarCStr, "EMPTY") == 0 || strcmp(lEnvVarCStr, "Empty") == 0)
      return boost::optional<CMFPGAType>();

    const auto lIt = kCMFPGAEnvValueMap.find(lEnvVarCStr);
    if (lIt != kCMFPGAEnvValueMap.end())
      return lIt->second;
    else {
      std::vector<std::string> lValidValues;
      for (const auto& x : kCMFPGAEnvValueMap)
        lValidValues.push_back(x.first);
      std::sort(lValidValues.begin(), lValidValues.end());

      std::ostringstream lExcMessage;
      lExcMessage << "Environment variable '" << lEnvVarName << "' has unexpected value '" << lEnvVarCStr << "' (valid values:";
      for (const auto& x : lValidValues)
        lExcMessage << " " << x;
      lExcMessage << ")";
      throw std::runtime_error(lExcMessage.str());
    }
  }
  else
    throw std::runtime_error("Environment variable '" + lEnvVarName + "' is not set");
}

CMBoardType getBoardType()
{
  if (const char* lEnvVarCStr = std::getenv("APOLLO_TYPE")) {
    if (strcmp(lEnvVarCStr, "Rev2b") == 0)
      return CMBoardType::Rev2b;
    else if (strcmp(lEnvVarCStr, "rev2b") == 0)
      return CMBoardType::Rev2b;
    else
      throw std::runtime_error("Environment variable 'APOLLO_TYPE' has invalid value '" + std::string(lEnvVarCStr) + "' (known values are: Rev2b, rev2b)");
  }
  else
      throw std::runtime_error("Environment variable 'APOLLO_TYPE' is not set '");
}

ChannelOrigin parseSpec(const CMFPGAType aCMFPGAType){
  const auto lFilePathIt = kCMFPGASpecPaths.find(aCMFPGAType);
  if (lFilePathIt == kCMFPGASpecPaths.end()) {
      throw std::runtime_error("No specification file has been declared for CMFPGA " + CMFPGATypeToString(aCMFPGAType));
  }

  std::string filePath = lFilePathIt->second;
  const char* configExt = std::getenv("CONFIG_EXT");

  // If CONFIG_EXT is not null and not empty, append it to the file path before ".yaml"
  if (configExt && std::string(configExt).length() > 0) {
      size_t pos = filePath.rfind(".yaml");
      if (pos != std::string::npos) {
          // Insert "_" + CONFIG_EXT before ".yaml"
          filePath.insert(pos, "_" + std::string(configExt));
      }
  }

  if (!boost::filesystem::exists(filePath)) {
      throw std::runtime_error("Config file path with provided extension does not exist: " + filePath);
  }

  std::ifstream file(filePath);

  if (!file.is_open()) {
      throw std::runtime_error("Failed to open file: " + filePath);
  }

  std::string yaml_data((std::istreambuf_iterator<char>(file)), std::istreambuf_iterator<char>());

  YAML::Node data = YAML::Load(yaml_data);

  ChannelOrigin channel_origin;

  Polarity default_polarity = Polarity::Normal;

  // Iterate through the firefly node
  for (const auto& kv : data["firefly"]) {
      for (const auto& sub_kv : kv.second) {
          YAML::Node sub_value = sub_kv.second;
          int calculated_key;
          int channel;

          // Depending on the FPGA, map the physical channel to the 'EMP' channel using the standard or equation or flipped equation for F2
          if (aCMFPGAType == CMFPGAType::VU13p_F1) {
            calculated_key = sub_value["Quad"]["EMP"].as<int>() * 4 + sub_value["Quad"]["Serdes"]["Channel"].as<int>();
          }
          else{
            calculated_key = sub_value["Quad"]["EMP"].as<int>() * 4 + 3 - sub_value["Quad"]["Serdes"]["Channel"].as<int>();
          }

          std::string Optical_ID = sub_value["Quad"]["Optical_ID"].as<std::string>();
          channel = sub_value["Quad"]["Serdes"]["Channel"].as<int>();

          // Check if 'XCVR' substring is present - if so register the same channel to the Rx and Tx of the bidirectional
          if (Optical_ID.find("XCVR") != std::string::npos) {
              channel_origin.rx[calculated_key] = {default_polarity, {Optical_ID, std::to_string(channel)}};
              channel_origin.tx[calculated_key] = {default_polarity, {Optical_ID, std::to_string(channel)}};
          }

          // Check if 'Uni' substring is present - if so register the same channel to the Rx and Tx component separately
          else if (Optical_ID.find("Uni") != std::string::npos) {
              // Replace 'Uni' with 'Rx' for channel_Rx
              std::string Optical_ID_Rx = Optical_ID;
              size_t pos = Optical_ID_Rx.find("Uni");
              if (pos != std::string::npos) {
                  Optical_ID_Rx.replace(pos, 3, "Rx12");
              }
              // Replace 'Uni' with 'Tx' for channel_Tx
              std::string Optical_ID_Tx = Optical_ID;
              pos = Optical_ID_Tx.find("Uni");
              if (pos != std::string::npos) {
                  Optical_ID_Tx.replace(pos, 3, "Tx12");
              }
              channel_origin.rx[calculated_key] = {default_polarity, {Optical_ID_Rx, std::to_string(channel)}};
              channel_origin.tx[calculated_key] = {default_polarity, {Optical_ID_Tx, std::to_string(channel)}};
          }
          // Check if 'Rx' substring is present - if so register only the Rx components
          else if (Optical_ID.find("Rx") != std::string::npos) {
              std::string Optical_ID_Rx = Optical_ID;
              channel_origin.rx[calculated_key] = {default_polarity, {Optical_ID_Rx, std::to_string(channel)}};
          }
          // Check if 'Tx' substring is present - if so register only the Tx components
          else if (Optical_ID.find("Tx") != std::string::npos) {
              std::string Optical_ID_Tx = Optical_ID;
              channel_origin.tx[calculated_key] = {default_polarity, {Optical_ID_Tx, std::to_string(channel)}};
          }
      }
  }

  // Iterate through the GTY node
  for (const auto& kv : data["gty"]) {
      for (const auto& sub_kv : kv.second) {
          YAML::Node sub_value = sub_kv.second;
          int calculated_key;
          int calculated_key_across;
          // Depending on the FPGA, map the physical channel to the 'EMP' channel using the standard or equation or flipped equation for F2
          // These are static and we are always mapping F1<=>F2. Calculated_key_across is the EMP channel for the FPGA across from the one currently being registered
          if (aCMFPGAType == CMFPGAType::VU13p_F1) {
            std::string FPGA_ID = "F2";
            calculated_key = sub_value["Quad"]["EMP"].as<int>() * 4 + sub_value["Quad"]["Serdes"]["Channel"].as<int>();
            calculated_key_across = sub_value["Quad"]["EMP_Across"].as<int>() * 4 + 3 - sub_value["Quad"]["Serdes"]["Channel"].as<int>();
            channel_origin.rx[calculated_key] = {default_polarity, {FPGA_ID, std::to_string(calculated_key_across)}};
            channel_origin.tx[calculated_key] = {default_polarity, {FPGA_ID, std::to_string(calculated_key_across)}};  
          }
          else{
            std::string FPGA_ID = "F1";
            calculated_key_across = sub_value["Quad"]["EMP_Across"].as<int>() * 4 + sub_value["Quad"]["Serdes"]["Channel"].as<int>();
            calculated_key = sub_value["Quad"]["EMP"].as<int>() * 4 + 3 - sub_value["Quad"]["Serdes"]["Channel"].as<int>();
            channel_origin.rx[calculated_key] = {default_polarity, {FPGA_ID, std::to_string(calculated_key_across)}};
            channel_origin.tx[calculated_key] = {default_polarity, {FPGA_ID, std::to_string(calculated_key_across)}};  
          }
      }
  }

  return channel_origin;
}

} // namespace apolloherd
} // namespace swatch
