#include "swatch/apolloherd/ApolloDevice.hpp"
#include "swatch/apolloherd/commands/ApolloAccess.hpp"
#include "swatch/apolloherd/commands/Program.hpp"
#include "swatch/core/Factory.hpp"
#include "swatch/apolloherd/ApolloDeviceController.hpp"
#include "swatch/apolloherd/ApolloTable.hpp"

// register device to factory
SWATCH_REGISTER_CLASS(swatch::apolloherd::ApolloDevice)

namespace swatch {
namespace apolloherd {

ApolloDevice::ApolloDevice(const core::AbstractStub& aStub) :
  ServiceModule(aStub),
  mController(getStub().uri, getStub().addressTable)
{
  
  // Register commands 
  auto& ApolloAccess = registerCommand<commands::ApolloAccess>("ApolloAccess");

  // Register tables
  mTableInformation.push_back( {"cm",         "CM",              "CM"        } );
  mTableInformation.push_back( {"cmMon",      "CM_MON",          "CM_MON"    } );
  mTableInformation.push_back( {"c2c",        "C2C",             "C2C"       } );
  mTableInformation.push_back( {"ipmc",       "IPMC",            "IPMC"      } );
  mTableInformation.push_back( {"firmware",   "Firmware",        "FIRMWARE"  } );
  mTableInformation.push_back( {"status",     "Status",          "STATUS"    } );
  mTableInformation.push_back( {"zynqOS",     "ZYNQ OS Table",   "ZYNQ_OS"   } );

  for (auto& t : mTableInformation) {
    addMonitorable( new ApolloTable(t[0], t[1], t[2], mController) );
  }
}

ApolloDevice::~ApolloDevice()
{
}

void ApolloDevice::retrieveMetricValues()
{
}
void ApolloDevice::resetDevice()
{
  mController.recreateDevice();
}

}   // apolloherd
}   // swatch

