#include "swatch/apolloherd/FireflyBase.hpp"

#include "swatch/core/MetricConditions.hpp"

namespace swatch {
namespace apolloherd {

FireflyBase::FireflyBase(const std::string& aId, const std::string& aAlias, const std::vector<size_t>& aRxIndices, const std::vector<size_t>& aTxIndices) :
  OpticalModule(aId, aAlias, aRxIndices, aTxIndices),
  //mPartStr(aFirefly.Attribute("device name")), //Accessed via SMASH
  //mPart(registerProperty<std::string>("Part")),
  //mSerialNumber(registerProperty<std::string>("Serial number")),
  //mFirmwareVersion(registerProperty<std::string>("Firmware version")),
  //mElementName(aAlias),
  mMetricTemperature(registerMetric<float>("Temperature")),
  mMetricOptPow(registerMetric<float>("Power")) //update to correct units
  //mMetricPeakTemperature(registerMetric<float>("peakTemperature"))
{
  //mPart.setDescription("Firefly part code");
  //mMetricPeakTemperature.setUnit("C");
  //mMetricPeakTemperature.setFormat(core::format::kFixedPoint, 1);
  mMetricTemperature.setUnit("C");
  mMetricTemperature.setFormat(core::format::kFixedPoint, 1);
  setWarningCondition(mMetricTemperature, ::swatch::core::GreaterThanCondition<float>(50));

  mMetricOptPow.setUnit("0.1 MicroWatts");
  mMetricOptPow.setFormat(core::format::kFixedPoint, 1);
  setWarningCondition(mMetricOptPow, ::swatch::core::GreaterThanCondition<float>(65535)); // 6.5535 mW / 0.1 microW

  std::vector<std::string> arg {"/opt/address_table/connections.xml"};
  SM = std::make_shared<ApolloSM>(arg);

}

FireflyBase::~FireflyBase()
{
}

void FireflyBase::retrievePropertyValues() {
  //
}

bool FireflyBase::checkPresence() const
{
  return true;
}

/* Adapt to Apollo
std::pair<float, std::string> FireflyBase::measurePhysicalQuantity(Element& aElement, const std::string& aName)
{
  // Implement
}


std::map<size_t, bool> FireflyBase::measureChannelFlag(Element& aElement, const std::string& aName, const std::vector<size_t> aChannels, const std::unordered_map<std::string, bool>& aInterpretationMap)
{
  // Implement
}


std::map<size_t, std::string> FireflyBase::measureChannelSetting(Element& aElement, const std::string& aName, const std::vector<size_t> aChannels)
{
  // Implement
}
*/

} // apolloherd
} // swatch
