#include "swatch/apolloherd/ApolloCMFPGA.hpp"

#include <boost/lexical_cast.hpp>
#include <boost/optional.hpp>

#include "swatch/action/StateMachine.hpp"
#include "swatch/core/Factory.hpp"

#include "swatch/apolloherd/commands/Program.hpp"
#include "swatch/apolloherd/commands/PowerUp.hpp"
#include "swatch/apolloherd/commands/PowerDown.hpp"
#include "swatch/apolloherd/commands/ApolloAccess.hpp"
#include "swatch/apolloherd/commands/dumpFFInfo.hpp"

#include <uhal/ConnectionManager.hpp>


SWATCH_REGISTER_CLASS(swatch::apolloherd::ApolloCMFPGA)

namespace swatch {
namespace apolloherd {

using namespace ::swatch;

/* Serenity Commands
const std::string DaughterCard::CmdIds::kConfigureClockSynths  = "configureClockSynths";
const std::string DaughterCard::CmdIds::kConfigureXpointSwitch = "configureCrosspointSwitch";
const std::string DaughterCard::CmdIds::kConfigureFireflyRx    = "configureFireflyRx";
const std::string DaughterCard::CmdIds::kConfigureFireflyTx    = "configureFireflyTx";
const std::string DaughterCard::CmdIds::kResetFireflys         = "resetFireflys";
const std::string DaughterCard::CmdIds::kPowerCycle = "powerCycle";
// Not Needed????
*/
const std::string ApolloCMFPGA::CmdIds::ApolloAccess = "apolloAccess";
const std::string ApolloCMFPGA::CmdIds::PowerDown = "powerDown";
const std::string ApolloCMFPGA::CmdIds::PowerUp  = "powerUp";
const std::string ApolloCMFPGA::CmdIds::Program  = "program";


ApolloCMFPGA::ApolloCMFPGA(const ::swatch::core::AbstractStub& aStub) :
  ApolloCMFPGA(aStub, getSiteFromId(aStub.id), parseSpec(getCMFPGAType(aStub.id)))
{
}


ApolloCMFPGA::ApolloCMFPGA(const ::swatch::core::AbstractStub& aStub, Site aSite, const ChannelOrigin& aChannelOrigin) :
  emp::swatch::Processor(aStub,
                         getNumberOfPorts(aSite),
                         0,
                         [&aChannelOrigin, &aSite] (const size_t i) { return extractRxConnection(i, aChannelOrigin); },
                         [&aChannelOrigin] (const size_t i) { return extractRxInversion(i, aChannelOrigin); },
                         [&aChannelOrigin, &aSite] (const size_t i) { return extractTxConnection(i, aChannelOrigin); },
                         [&aChannelOrigin] (const size_t i) { return extractTxInversion(i, aChannelOrigin); }),
  mSite(aSite),
  mFireflyRxPolarity(createFireflyPolarityMapRx(aChannelOrigin)),
  mFireflyTxPolarity(createFireflyPolarityMapTx(aChannelOrigin))
  //mDCType retrievePropertyValues
  //mDCUID retrievePropertyValues
  //mPowerMetric retrieveMetricValues
{
  if (aStub.id == "F2")
    setTCDS2Source("F1");
  SM = std::make_shared<ApolloSM>(arg);
  SMDevice = std::make_unique<BUTool::ApolloSMDevice>(SM);
  
  registerCommand<commands::Program>(CmdIds::Program);
  registerCommand<commands::PowerUp>(CmdIds::PowerUp);
  registerCommand<commands::PowerDown>(CmdIds::PowerDown);
  registerCommand<commands::dumpFFInfo>("dumpFFInfo");

  getPayloadTestFSM().setup
      .add(getCommand(CmdIds::PowerUp))
      .add(getCommand(CmdIds::Program));
  emp::swatch::populateTransitions(getPayloadTestFSM());

  getLinkTestFSM().setup
      .add(getCommand(CmdIds::PowerUp))
      .add(getCommand(CmdIds::Program));
  emp::swatch::populateTransitions(getLinkTestFSM());

}
ApolloCMFPGA::~ApolloCMFPGA()
{
  // if (NULL != SMDevice) {
  //   delete SMDevice;
  // }
}

void ApolloCMFPGA::recreateSMDevice()
{
  // clear uHal cashed address tables before creating a new device
  uhal::ConnectionManager::clearAddressFileCache();
  // Recreate SM
  SM.reset(new ApolloSM(arg));
  // Recreate SMDevice
  SMDevice.reset(new BUTool::ApolloSMDevice(SM));

}

Site ApolloCMFPGA::getSite() const
{
  return mSite;
}

bool ApolloCMFPGA::checkPresence() const
{
  // Always returning true since if CMFPGA is absent then ProcessorAbsent exception would have been thrown in construction
  return true;
}


Site ApolloCMFPGA::getSiteFromId(const std::string &aId)
{
  if (aId == "F1")
    return Site::F1;
  else if (aId == "F2")
    return Site::F2;
  else
    throw std::runtime_error("Invalid ID '" + aId + "' for ApolloCMFPGA object (must be either 'F1' or 'F2')");
}

CMFPGAType ApolloCMFPGA::getCMFPGAType(const std::string& aId)
{
  return getCMFPGAType(getSiteFromId(aId));
}

CMFPGAType ApolloCMFPGA::getCMFPGAType(const Site aSite)
{
  const auto lResult = getApolloCMFPGAType(aSite);

  // If ApolloCMFPGA absent then throw 'ProcessorAbsent' exception to declare this to framework
  if (lResult == boost::none)
    throw ::swatch::phase2::ProcessorAbsent("No CMFPGA is situated at " + SiteToString(aSite));

  return *lResult;
}

const std::map<CMFPGAType, size_t> ApolloCMFPGA::kPortCountMap = {
  {CMFPGAType::VU13p_F1, 128},
  {CMFPGAType::VU13p_F2, 128}
};

size_t ApolloCMFPGA::getNumberOfPorts(const Site aSite)
{
  const auto lCMFPGAType = getCMFPGAType(aSite);

  const auto lIt = kPortCountMap.find(lCMFPGAType);
  if (lIt != kPortCountMap.end()){
    return lIt->second;
  }
  else{
    throw std::runtime_error("Could not determine number of I/O ports for CMFPGA " + CMFPGATypeToString(lCMFPGAType));
    return -999;
  }
}

boost::optional<::swatch::phase2::ChannelID> ApolloCMFPGA::extractRxConnection(const size_t i, const ChannelOrigin& aChannelOrigin){
    log4cplus::Logger lLogger(log4cplus::Logger::getInstance("swatch.apolloherd.ApolloCMFPGA"));
    boost::optional<::swatch::phase2::ChannelID> lResult;
    const auto lIt = aChannelOrigin.rx.find(i);
    if (lIt != aChannelOrigin.rx.end()) {
        const auto& vectorInPair = lIt->second.second;
        std::string lFireflySiteName = vectorInPair[0];
        unsigned const int lFireflyChannelIndex = std::stoi(vectorInPair[1]);
        lResult = ::swatch::phase2::ChannelID{lFireflySiteName, lFireflyChannelIndex};
        LOG4CPLUS_DEBUG(lLogger, "extractRxConnection(i=" << i << ") -> Firefly " << lFireflySiteName << ", channel " << lFireflyChannelIndex);
    }
    else{
        LOG4CPLUS_DEBUG(lLogger, "extractRxConnection(i=" << i << ") -> Channel not connected");
    }
    return lResult;
}

boost::optional<::swatch::phase2::ChannelID> ApolloCMFPGA::extractTxConnection(const size_t i, const ChannelOrigin& aChannelOrigin){
    log4cplus::Logger lLogger(log4cplus::Logger::getInstance("swatch.apolloherd.ApolloCMFPGA"));
    boost::optional<::swatch::phase2::ChannelID> lResult;
    const auto lIt = aChannelOrigin.tx.find(i);
    if (lIt != aChannelOrigin.tx.end()) {
        const auto& vectorInPair = lIt->second.second;
        std::string lFireflySiteName = vectorInPair[0];
        unsigned const int lFireflyChannelIndex = std::stoi(vectorInPair[1]);
        lResult = ::swatch::phase2::ChannelID{lFireflySiteName, lFireflyChannelIndex};
        LOG4CPLUS_DEBUG(lLogger, "extractTxConnection(i=" << i << ") -> Firefly " << lFireflySiteName << ", channel " << lFireflyChannelIndex);
    }
    else{
        LOG4CPLUS_DEBUG(lLogger, "extractTxConnection(i=" << i << ") -> Channel not connected");
    }
    return lResult;
}

bool ApolloCMFPGA::extractRxInversion(const size_t i, const ChannelOrigin& aChannelOrigin) // Implement Exception Catching
{
  const auto lIt = aChannelOrigin.rx.find(i);
  if (lIt != aChannelOrigin.rx.end()){
    Polarity inversion = lIt->second.first;
    return inversion == Polarity::Invert; // Default is True for Inverted, False for Normal
  }
  else{
    return false; // Assuming "Normal" polarity
  }
}

bool ApolloCMFPGA::extractTxInversion(const size_t i, const ChannelOrigin& aChannelOrigin) // Implement Exception Catching
{
  const auto lIt = aChannelOrigin.tx.find(i);
  if (lIt != aChannelOrigin.tx.end()){
    Polarity inversion = lIt->second.first;
    return inversion == Polarity::Invert; // Default is True for Inverted, False for Normal
  }
  else{
    return false; // Assuming "Normal" polarity
  }
}

ApolloCMFPGA::FireflyPolarityMap_t ApolloCMFPGA::createFireflyPolarityMapRx(const ChannelOrigin& aChannelOrigin)
{
  std::map<::swatch::phase2::ChannelID, Polarity, DummyComparator> lResult;
  for (const auto& x : aChannelOrigin.rx) {
    const auto& vectorInPair = x.second.second;
    std::string lFireflySiteName = vectorInPair[0];
    const int lFireflyChannelIndex = std::stoi(vectorInPair[1]);
    ::swatch::phase2::ChannelID lChannelID = ::swatch::phase2::ChannelID{lFireflySiteName, lFireflyChannelIndex};
    lResult[lChannelID] = x.second.first;
  }
  return lResult;
}

ApolloCMFPGA::FireflyPolarityMap_t ApolloCMFPGA::createFireflyPolarityMapTx(const ChannelOrigin& aChannelOrigin)
{
  std::map<::swatch::phase2::ChannelID, Polarity, DummyComparator> lResult;
  for (const auto& x : aChannelOrigin.tx) {
    const auto& vectorInPair = x.second.second;
    std::string lFireflySiteName = vectorInPair[0];
    const int lFireflyChannelIndex = std::stoi(vectorInPair[1]);
    ::swatch::phase2::ChannelID lChannelID = ::swatch::phase2::ChannelID{lFireflySiteName, lFireflyChannelIndex};
    lResult[lChannelID] = x.second.first;
  }
  return lResult;
}

Polarity ApolloCMFPGA::getFireflyRxPolarity(const std::string& aName, const size_t i) const
{
  ::swatch::phase2::ChannelID channelId = {aName, i};

  const auto lIt = mFireflyRxPolarity.find(channelId);

  if (lIt == mFireflyRxPolarity.end())
    throw std::runtime_error("Could not determine polarity for Firefly '" + aName + "', rx channel " + std::to_string(i));

  return lIt->second;
}

Polarity ApolloCMFPGA::getFireflyTxPolarity(const std::string& aName, const size_t i) const
{
  ::swatch::phase2::ChannelID channelId = {aName, i};

  const auto lIt = mFireflyTxPolarity.find(channelId);

  if (lIt == mFireflyTxPolarity.end())
    throw std::runtime_error("Could not determine polarity for Firefly '" + aName + "', rx channel " + std::to_string(i));

  return lIt->second;
}

std::string ApolloCMFPGA::dumpFFInfo() {
  std::stringstream result;

  result << "Size of mFireflyRxPolarity: " << mFireflyRxPolarity.size() << "\n";
  result << "This is mFireflyRxPolarity: \n";
  for (const auto& entry : mFireflyRxPolarity) {
    const ::swatch::phase2::ChannelID& channelID = entry.first;
    result << "(" << channelID.component << ", " << channelID.index << ")\n";
  }

  result << "Size of mFireflyTxPolarity: " << mFireflyTxPolarity.size() << "\n";
  result << "This is mFireflyTxPolarity: \n";
  for (const auto& entry : mFireflyTxPolarity) {
    const ::swatch::phase2::ChannelID& channelID = entry.first;
    result << "(" << channelID.component << ", " << channelID.index << ")\n";
  }

  std::string resultStr = result.str();
  if (!resultStr.empty()) {
    resultStr.resize(resultStr.size() - 1);
  }

  return resultStr;
}

int ApolloCMFPGA::ApolloAccess(std::string command_args) {
  std::istringstream iss(command_args);
  std::vector<std::string> CommandArgs;
  std::copy(std::istream_iterator<std::string>(iss),
            std::istream_iterator<std::string>(),
            std::back_inserter(CommandArgs));
  fprintf(stderr, "%p\n", SMDevice.get());
  return SMDevice->EvaluateCommand(CommandArgs);
}

void ApolloCMFPGA::AddStream(Level::level level, std::ostream* os) {
  SMDevice->AddOutputStream(level, os);
}

void ApolloCMFPGA::RemoveStream(Level::level level, std::ostream* os) {
  SMDevice->RemoveOutputStream(level, os);
}

int ApolloCMFPGA::svfPlayer(std::string const & svfFile, std::string const & XVCReg) {
  return SM->svfplayer(svfFile, XVCReg);
}

uint32_t ApolloCMFPGA::ReadRegister(std::string const & reg){
  return SM->ReadRegister(reg);
}

void ApolloCMFPGA::WriteRegister(std::string const & reg, uint32_t value){
  SM->WriteRegister(reg, value);
}

void ApolloCMFPGA::unblockAxi(std::string name){
  SM->unblockAXI(name);
}

void ApolloCMFPGA::retrieveMetricValues()
{
  emp::swatch::Processor::retrieveMetricValues();
}

void ApolloCMFPGA::retrievePropertyValues()
{
}


} // apolloherd
} // swatch
