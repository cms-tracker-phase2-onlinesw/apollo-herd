#include "swatch/apolloherd/ApolloTable.hpp"

namespace swatch {
namespace apolloherd {

ApolloTable::ApolloTable(std::string const & aId, std::string const & aAlias, std::string const & tableName, ApolloDeviceController & controller) :
    MonitorableObject(aId, aAlias),
    mController(controller)
{
    // Get list of register names for this table (registers with status=1, by default)
    mRegisterNames = mController.GetRegisterNamesFromTable(tableName);

    for (std::string registerName : mRegisterNames) {
        // Get the correct convert type for this register
        ConvertType convertType = mController.GetConvertType(registerName);
    
        // Store ApolloMetric instances in an array as a class member
        ApolloMetric metric = ApolloMetric(convertType, registerName);
        mApolloMetrics.push_back(metric);

        std::string registerId = metric.getRegisterId();

        // Register the metric with the correct data type
        switch(convertType){
            case BUTool::RegisterHelperIO::FP:
                registerMetric<double>(registerId, registerName);
                break;
            case BUTool::RegisterHelperIO::INT:
                registerMetric<int64_t>(registerId, registerName);
                break;
            case BUTool::RegisterHelperIO::STRING:
                registerMetric<std::string>(registerId, registerName);
                break;
            case BUTool::RegisterHelperIO::UINT:
            case BUTool::RegisterHelperIO::NONE:
            default:
                registerMetric<uint64_t>(registerId, registerName);
                break;
            }
    }
}

ApolloTable::~ApolloTable()
{
}

template<typename T>
void ApolloTable::readAndSetMetric(ApolloMetric const & metric)
{
    /* 
    Get the corresponding SimpleMetric, read the converted value and
    set the value of the SimpleMetric instance.
    */
    std::string registerName = metric.getRegisterName();
    std::string registerId = metric.getRegisterId();

    SimpleMetric<T>& mSimpleMetric = getMetric<T>(registerId);
    T val;

    // Read the value using BUTool, and set the value of the corresponding SimpleMetric
    // If we encounter a BUS_ERROR, just skip this register
    try {
        mController.ReadConvert(registerName, val);
        setMetric(mSimpleMetric, val);
    } catch (BUException::BUS_ERROR & e) {
        fprintf(stderr, "BUS_ERROR encountered, skipping register: %s\n", registerName.c_str());
    }
}

void ApolloTable::retrieveMetricValues()
{
    /*
     * Function called to retrieve and update values of registered metrics within this table.
     * Depending on the data type of the metric, will call the appropriate readAndSetMetric() function.
     */
    for (ApolloMetric& metric : mApolloMetrics) {
        // The data type for this metric
        ConvertType convertType = metric.getConvertType();
        
        switch(convertType) {
            case BUTool::RegisterHelperIO::STRING:
                readAndSetMetric<std::string>(metric);
                break;
            case BUTool::RegisterHelperIO::FP:
                readAndSetMetric<double>(metric);
                break;
            case BUTool::RegisterHelperIO::INT:
                readAndSetMetric<int64_t>(metric);
                break;
            case BUTool::RegisterHelperIO::UINT:
            case BUTool::RegisterHelperIO::NONE:
            default:
                readAndSetMetric<uint64_t>(metric);
                break;
        }
    }
}


} // apolloherd
} // swatch