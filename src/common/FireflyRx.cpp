#include "swatch/apolloherd/FireflyRx.hpp"

#include "swatch/core/MetricConditions.hpp"


namespace swatch {
namespace apolloherd {

using namespace ::swatch;

const std::vector<size_t> FireflyRx::kChannels{ 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11 };

FireflyRx::FireflyRx(const std::string& aId) :
  FireflyRx(aId, aId)
{
}

FireflyRx::FireflyRx(const std::string& aId, const std::string& aAlias) :
  FireflyBase(aId, aAlias, kChannels, { }),
  FF_name(aId)
{
  /* Adapt to Apollo
  for (auto* channel: getInputs()) {
    mPropertyState[channel] = &channel->registerProperty<std::string>("State");
    mPropertyAmplitude[channel] = &channel->registerProperty<std::string>("Amplitude");
    mPropertyPolarity[channel] = &channel->registerProperty<std::string>("Polarity");
    mPropertyPreEmph[channel] = &channel->registerProperty<float>("Pre-emphasis");
    mPropertySquelch[channel] = &channel->registerProperty<std::string>("Squelch");
    mPropertyCDR[channel] = &channel->registerProperty<std::string>("CDR");

    mPropertyPreEmph.at(channel)->setUnit("dB");
    mPropertyPreEmph.at(channel)->setFormat(core::format::kFixedPoint, 1);

    mMetricPower[channel] = &channel->registerMetric<float>("power", "Power");
    mMetricLOL[channel] = &channel->registerMetric<bool>("LOL", "CDR lock lost (LOL)", ::swatch::core::EqualCondition<bool>(true));
    mMetricLOS[channel] = &channel->registerMetric<bool>("LOS", "Signal lost (LOS)", ::swatch::core::EqualCondition<bool>(true));

    mMetricPower.at(channel)->setUnit("mW");
    mMetricPower.at(channel)->setFormat(core::format::kFixedPoint, 6);
    setWarningCondition(*mMetricPower.at(channel), swatch::core::LessThanCondition<float>(0.13));
    
    // Declare that this monitorable object will update each of the above per-channel metrics
    channel->delegateMetricUpdate(mMetricPower.at(channel)->getId(), *this);
    channel->delegateMetricUpdate(mMetricLOL.at(channel)->getId(), *this);
    channel->delegateMetricUpdate(mMetricLOS.at(channel)->getId(), *this);
  }
  */
}


FireflyRx::~FireflyRx()
{
}


void FireflyRx::retrieveInputPropertyValues(Channel& aChannel)
{
  /* Adapt to Apollo
  std::unique_ptr<Smash> lSmash(startSmashSession());
  auto& lFirefly(*lSmash->GetElement(mElementName));

  const auto lEnabled1 = measureChannelFlag(lFirefly, "Channel", kChannels, kEnableValueMap);
  const auto lEnabled2 = measureChannelFlag(lFirefly, "Output", kChannels, kEnableValueMap);
  const auto lAmplitude = measureChannelSetting(lFirefly, "Amplitude", kChannels);
  const auto lInvPolarity = measureChannelFlag(lFirefly, "Polarity", kChannels, kPolarityInvertedValueMap);
  const auto lSquelch = measureChannelFlag(lFirefly, "Squelch", kChannels, kEnableValueMap);
  const auto lCDR = measureChannelFlag(lFirefly, "CDR", kChannels, kEnableValueMap);
  const auto lPreEmphStrMap = measureChannelSetting(lFirefly, "Pre-emphasis", kChannels);

  if (lEnabled1.at(aChannel.getIndex()) and lEnabled2.at(aChannel.getIndex()))
    set<std::string>(*mPropertyState.at(&aChannel), "Enabled");
  else if (lEnabled1.at(aChannel.getIndex()) or lEnabled2.at(aChannel.getIndex()))
    set<std::string>(*mPropertyState.at(&aChannel), "Mixed");
  else
    set<std::string>(*mPropertyState.at(&aChannel), "Disabled");

  set<std::string>(*mPropertyAmplitude.at(&aChannel), lAmplitude.at(aChannel.getIndex()));
  set<std::string>(*mPropertyPolarity.at(&aChannel), std::string(lInvPolarity.at(aChannel.getIndex()) ? "Inverted" : "Normal"));
  set<std::string>(*mPropertySquelch.at(&aChannel), lSquelch.at(aChannel.getIndex()) ? "Enabled" : "Disabled");
  set<std::string>(*mPropertyCDR.at(&aChannel), lCDR.at(aChannel.getIndex()) ? "Enabled" : "Disabled");

  float lPreEmphValue = 999999.0;
  if (lPreEmphStrMap.at(aChannel.getIndex()) == "Max")
    lPreEmphValue = 7.5;
  else if (lPreEmphStrMap.at(aChannel.getIndex()) == "Min")
    lPreEmphValue = 0.0;
  else {
    const std::string lPreEmphStr = lPreEmphStrMap.at(aChannel.getIndex()).substr(0, lPreEmphStrMap.at(aChannel.getIndex()).size() - 2);
    if (std::count(lPreEmphStr.begin(), lPreEmphStr.end(), '.') > 1)
      throw std::runtime_error("Too many '.' in pre-emphasis measurement \"" + lPreEmphStrMap.at(aChannel.getIndex()) + "\"");
    if (lPreEmphStr.find_first_not_of("0123456789.") == std::string::npos)
      throw std::runtime_error("Invalid characters found in pre-emphasis measurement \"" + lPreEmphStrMap.at(aChannel.getIndex()) + "\"");
    lPreEmphValue = std::stof(lPreEmphStr);
  }
  set(*mPropertyPreEmph.at(&aChannel), lPreEmphValue);
   */
}


void FireflyRx::retrieveMetricValues()
{
  /* Adapt to Apollo
  std::unique_ptr<Smash> lSmash(startSmashSession());
  auto& lFirefly(*lSmash->GetElement(mElementName));

  
  setMetric(mMetricPeakTemperature, measurePhysicalQuantity(lFirefly, "Peak Temperature").first);

  const auto lLOL = measureChannelFlag(lFirefly, "CDR LOL alarms", kChannels, kAlarmValueMap);
  const auto lLOS = measureChannelFlag(lFirefly, "LOS alarms", kChannels, kAlarmValueMap);

  for (auto* channel: getInputs()) {
    std::stringstream lPowerMeasurementName;
    lPowerMeasurementName << "RSSI ";
    lPowerMeasurementName << std::setw(2) << std::setfill('0') << channel->getIndex();

    setMetric(*mMetricPower.at(channel), float(measurePhysicalQuantity(lFirefly, lPowerMeasurementName.str()).first / 1000.0));
    setMetric(*mMetricLOL.at(channel), lLOL.at(channel->getIndex()));
    setMetric(*mMetricLOS.at(channel), lLOS.at(channel->getIndex()));
  }
  */

  setMetric(mMetricTemperature, static_cast<float>(SM->ReadRegister("PL_MEM_CM." + FF_name + ".Temp_C")));
  setMetric(mMetricOptPow, static_cast<float>(SM->ReadRegister("PL_MEM_CM." + FF_name + ".OptPow")));
}


void FireflyRx::retrieveInputMetricValues(Channel& aChannel)
{
  // Monitoring data for input channels updated by parent instead (since called delegateUpdateMetrics in CTOR)
}



} // apolloherd
} // swatch
