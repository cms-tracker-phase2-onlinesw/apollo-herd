#include "swatch/apolloherd/FireflyBi.hpp"

#include "swatch/core/MetricConditions.hpp"

#include <iostream>

namespace swatch {
namespace apolloherd {

using namespace ::swatch;

FireflyBi::FireflyBi(const std::string& aId) :
  FireflyBi(aId, aId)
{
}

FireflyBi::FireflyBi(const std::string& aId, const std::string& aAlias) :
  FireflyBase(aId, aAlias, {0, 1, 2, 3}, {0, 1, 2, 3}),
  FF_name(aId)
{
  /* Adapt to Apollo 
  for (auto* channel: getInputs()) {
    mMetricRxPower[channel] = &channel->registerMetric<float>("power", "Power");
    mMetricRxLOL[channel] = &channel->registerMetric<bool>("LOL", "CDR lock lost (LOL)", ::swatch::core::EqualCondition<bool>(true));
    mMetricRxLOS[channel] = &channel->registerMetric<bool>("LOS", "Signal lost (LOS)", ::swatch::core::EqualCondition<bool>(true));

    mMetricRxPower.at(channel)->setUnit("mW");
    mMetricRxPower.at(channel)->setFormat(core::format::kFixedPoint, 6);
    setWarningCondition(*mMetricRxPower.at(channel), swatch::core::LessThanCondition<float>(0.13));

    // Declare that this monitorable object will update each of the above per-channel metrics
    channel->delegateMetricUpdate(mMetricRxPower.at(channel)->getId(), *this);
    channel->delegateMetricUpdate(mMetricRxLOL.at(channel)->getId(), *this);
    channel->delegateMetricUpdate(mMetricRxLOS.at(channel)->getId(), *this);
  }

  for (auto* channel: getOutputs()) {
    mMetricTxLOL[channel] = &channel->registerMetric<bool>("LOL", "CDR lock lost (LOL)", ::swatch::core::EqualCondition<bool>(true));
    mMetricTxLOS[channel] = &channel->registerMetric<bool>("LOS", "Signal lost (LOS)", ::swatch::core::EqualCondition<bool>(true));
    mMetricTxLaserFault[channel] = &channel->registerMetric<bool>("laserFault", "Laser fault", ::swatch::core::EqualCondition<bool>(true));

    // Declare that this monitorable object will update each of the above per-channel metrics
    channel->delegateMetricUpdate(mMetricTxLOL.at(channel)->getId(), *this);
    channel->delegateMetricUpdate(mMetricTxLOS.at(channel)->getId(), *this);
    channel->delegateMetricUpdate(mMetricTxLaserFault.at(channel)->getId(), *this);
  }
  */
}


FireflyBi::~FireflyBi()
{
}


void FireflyBi::retrieveMetricValues()
{
  /* Adapt to Apollo 
  std::unique_ptr<Smash> lSmash(startSmashSession());
  auto& lFirefly(*lSmash->GetElement(mElementName));

  setMetric(mMetricPeakTemperature, measurePhysicalQuantity(lFirefly, "Peak Temperature").first);

  const auto lRxLOL = measureChannelFlag(lFirefly, "Rx LOL alarms", { 1, 2, 3, 4 }, kAlarmValueMap);
  const auto lRxLOS = measureChannelFlag(lFirefly, "Rx LOS alarms", { 1, 2, 3, 4 }, kAlarmValueMap);
  const auto lTxLOL = measureChannelFlag(lFirefly, "Tx LOL alarms", { 1, 2, 3, 4 }, kAlarmValueMap);
  const auto lTxLOS = measureChannelFlag(lFirefly, "Tx LOS alarms", { 1, 2, 3, 4 }, kAlarmValueMap);
  const auto lTxLaserFault = measureChannelFlag(lFirefly, "Tx Laser fault alarms", { 1, 2, 3, 4 }, kAlarmValueMap);

  for (auto* channel: getInputs()) {
    setMetric(*mMetricRxPower.at(channel), measurePhysicalQuantity(lFirefly, "Optical Power Rx" + std::to_string(channel->getIndex())).first);
    setMetric(*mMetricRxLOL.at(channel), lRxLOL.at(channel->getIndex()));
    setMetric(*mMetricRxLOS.at(channel), lRxLOS.at(channel->getIndex()));
  }

  for (auto* channel: getOutputs()) {
    setMetric(*mMetricTxLOL.at(channel), lTxLOL.at(channel->getIndex()));
    setMetric(*mMetricTxLOS.at(channel), lTxLOS.at(channel->getIndex()));
    setMetric(*mMetricTxLaserFault.at(channel), lTxLaserFault.at(channel->getIndex()));
  }
  */
  setMetric(mMetricTemperature, static_cast<float>(SM->ReadRegister("PL_MEM_CM." + FF_name + ".Temp_C")));
  setMetric(mMetricOptPow, static_cast<float>(SM->ReadRegister("PL_MEM_CM." + FF_name + ".OptPow")));
}


void FireflyBi::retrieveInputMetricValues(Channel& aChannel)
{
  // Monitoring data for input channels updated by parent instead (since called delegateUpdateMetrics in CTOR)
}


void FireflyBi::retrieveOutputMetricValues(Channel& aChannel)
{
  // Monitoring data for output channels updated by parent instead (since called delegateUpdateMetrics in CTOR)
}


} // apolloherd
} // swatch
