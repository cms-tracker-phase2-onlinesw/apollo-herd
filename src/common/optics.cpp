#include <map>
#include <stdexcept>
#include <bitset>
#include <unordered_map>

#include "swatch/phase2/Board.hpp"
#include "swatch/phase2/EmptySocket.hpp"

#include <ApolloSM_device/ApolloSM_device.hh>

#include "swatch/apolloherd/ApolloCMFPGA.hpp"
#include "swatch/apolloherd/utilities.hpp"
#include "swatch/apolloherd/FireflyBi.hpp"
#include "swatch/apolloherd/FireflyRx.hpp"
#include "swatch/apolloherd/FireflyTx.hpp"


//#include "swatch/apolloherd/FireflyBase.hpp" To be implemented

namespace swatch {
namespace apolloherd {

using ::swatch::phase2::Board;
using ::swatch::phase2::OpticalModule;

const std::map<CMBoardType, std::vector<std::string>> kUniBiFFs {
    {CMBoardType::Rev2b, {"P1_Tx12", "P1_Rx12", "P2_Tx12", "P2_Rx12", "P3_Tx12", "P3_Rx12", "P4_XCVR4", "P5_XCVR4", "P6_XCVR4", "P7_XCVR4"}},
};

const std::map<CMBoardType, std::vector<std::string>> isPresent {
    {CMBoardType::Rev2b, {"FFL12CH.IS_PRESENT", "FFLDAQ.IS_PRESENT"}},
};


std::map<std::string, uint32_t> generateBitmaskMap() {

    std::map<std::string, uint32_t> bitmaskMap;

    if (getBoardType() == CMBoardType::Rev2b) {
        for (int i = 0; i < 3; ++i) {
            bitmaskMap["P" + std::to_string(i + 1) + "_TX12"] = 1 << (2*i + 1);
            bitmaskMap["P" + std::to_string(i + 1) + "_RX12"] = 1 << (2*i);
        }

        for (int i = 0; i < 4; ++i) {
            bitmaskMap["P" + std::to_string(i + 4) + "_XCVR4"] = 1 << i;
        }
    }

    return bitmaskMap;
}

bool isBitSet(const std::string& registerName, uint32_t bitmask, std::shared_ptr<ApolloSM> SM) {
    uint32_t registerValue = SM->ReadRegister(registerName);

    return (registerValue & bitmask) != 0;
}

OpticalModule* createFireflyInstance(const std::string& aId)
{
  if (aId.find("XCVR") != std::string::npos){
    return new FireflyBi(aId);
  }
  else if (aId.find("Rx") != std::string::npos){
    return new FireflyRx(aId);
  }
  else if (aId.find("Tx") != std::string::npos){
    return new FireflyTx(aId);
  }
  else
    throw std::runtime_error("Firefly '" + aId + "' not recognized.");
}

OpticalModule* createEmptySocketInstance(const std::string& aId)
{
  using ::swatch::phase2::EmptySocket;

  if (aId.find("XCVR") != std::string::npos){
    return new EmptySocket(aId, aId, {0, 1, 2, 3}, {0, 1, 2, 3});
  }
  else if (aId.find("Rx") != std::string::npos){
    return new EmptySocket(aId, aId, 12, 0);
  }
  else if (aId.find("Tx") != std::string::npos){
    return new EmptySocket(aId, aId, 0, 12);
  }
  else
    throw std::runtime_error("Firefly '" + aId + "' not recognized.");
}

std::pair<std::vector<::swatch::phase2::OpticalModule*>, std::vector<std::string>> createOpticalModules()
{
  std::vector<std::string> arg {"/opt/address_table/connections.xml"};
  std::shared_ptr<ApolloSM> SM = std::make_shared<ApolloSM>(arg);

  CMBoardType boardType = getBoardType();

  std::vector<OpticalModule*> lModules;

  const auto lUniBiSpecs = kUniBiFFs.find(boardType);

  if (lUniBiSpecs == kUniBiFFs.end())
    throw std::runtime_error("No Uni/Bidirectional FF information has been declared for Board " + CMBoardTypeToString(boardType));

  const auto isPresentRegisters = isPresent.find(boardType);

  if (isPresentRegisters == isPresent.end())
    throw std::runtime_error("No FF presence information has been declared for Board " + CMBoardTypeToString(boardType));

  std::map<std::string, uint32_t> bitmaskMap = generateBitmaskMap();

  for (int i = 1; i <= 2; ++i) {
    for (const auto& value : lUniBiSpecs->second) {
      uint32_t bitmask = bitmaskMap[value];
      if (value.find("Rx") != std::string::npos || value.find("Tx") != std::string::npos){
        std::string registerName = "PL_MEM_CM.F"+std::to_string(i)+"_"+isPresentRegisters->second[0]; // PL_MEM_CM.Fi_FFL12CH.IS_PRESENT
        if(isBitSet(registerName, bitmask, SM)){
          lModules.push_back(createFireflyInstance("F"+std::to_string(i)+"_"+value));
        }
        else{
          lModules.push_back(createEmptySocketInstance("F"+std::to_string(i)+"_"+value));
        }
      }
      else{
        std::string registerName = "PL_MEM_CM.F"+std::to_string(i)+"_"+isPresentRegisters->second[1]; // "PL_MEM_CM.Fi_FFLDAQ.IS_PRESENT"
        if(isBitSet(registerName, bitmask, SM)){
          lModules.push_back(createFireflyInstance("F"+std::to_string(i)+"_"+value));
        }
        else{
          lModules.push_back(createEmptySocketInstance("F"+std::to_string(i)+"_"+value));
        }
      }
    }
  }

  return {lModules, lUniBiSpecs->second}; // second entry should be the name of the FrontPanel connectors?? dummy for now
}

} // apolloherd
} // swatch


SWATCH_REGISTER_PHASE2_OPTICS_CREATOR("apollo", swatch::apolloherd::createOpticalModules);
